object dmDatabase: TdmDatabase
  OldCreateOrder = False
  Height = 354
  Width = 418
  object SweetlifeConnection: TSQLConnection
    ConnectionName = 'SweetLife'
    DriverName = 'Firebird'
    LoginPrompt = False
    Params.Strings = (
      'DriverName=Firebird'
      'DriverUnit=Data.DBXFirebird'
      
        'DriverPackageLoader=TDBXDynalinkDriverLoader,DbxCommonDriver250.' +
        'bpl'
      
        'DriverAssemblyLoader=Borland.Data.TDBXDynalinkDriverLoader,Borla' +
        'nd.Data.DbxCommonDriver,Version=24.0.0.0,Culture=neutral,PublicK' +
        'eyToken=91d62ebb5b0d1b1b'
      
        'MetaDataPackageLoader=TDBXFirebirdMetaDataCommandFactory,DbxFire' +
        'birdDriver250.bpl'
      
        'MetaDataAssemblyLoader=Borland.Data.TDBXFirebirdMetaDataCommandF' +
        'actory,Borland.Data.DbxFirebirdDriver,Version=24.0.0.0,Culture=n' +
        'eutral,PublicKeyToken=91d62ebb5b0d1b1b'
      'LibraryName=dbxfb.dll'
      'LibraryNameOsx=libsqlfb.dylib'
      'VendorLib=fbclient.dll'
      'VendorLibWin64=fbclient.dll'
      'VendorLibOsx=/Library/Frameworks/Firebird.framework/Firebird'
      
        'Database=Z:\GIT\RadStudioLabs2\11-labSweetLife\Server\SWEETLIFE.' +
        'fdb'
      'User_Name=sysdba'
      'Password=masterkey'
      'Role=RoleName'
      'MaxBlobSize=-1'
      'LocaleCode=0000'
      'IsolationLevel=ReadCommitted'
      'SQLDialect=3'
      'CommitRetain=False'
      'WaitOnLocks=True'
      'TrimChar=False'
      'BlobSize=-1'
      'ErrorResourceFile='
      'RoleName=RoleName'
      'ServerCharSet=UTF8'
      'Trim Char=False')
    AfterConnect = SweetlifeConnectionAfterConnect
    BeforeConnect = SweetlifeConnectionBeforeConnect
    Connected = True
    Left = 199
    Top = 28
  end
  object Order_list_insProc: TSQLStoredProc
    MaxBlobSize = -1
    Params = <
      item
        DataType = ftInteger
        Precision = 4
        Name = 'PRODUCT_ID'
        ParamType = ptInput
      end
      item
        DataType = ftDate
        Precision = 4
        Name = 'DATE'
        ParamType = ptInput
      end
      item
        DataType = ftWideString
        Precision = 70
        Name = 'FIO'
        ParamType = ptInput
      end
      item
        DataType = ftWideString
        Precision = 20
        Name = 'PHONE'
        ParamType = ptInput
      end
      item
        DataType = ftWideString
        Precision = 255
        Name = 'NOTES'
        ParamType = ptInput
      end>
    SQLConnection = SweetlifeConnection
    StoredProcName = 'ORDER_LIST_INS'
    Left = 175
    Top = 212
  end
  object taOrderList: TSQLDataSet
    CommandText = 'ORDER_LIST'
    CommandType = ctTable
    DbxCommandType = 'Dbx.Table'
    MaxBlobSize = -1
    Params = <>
    SQLConnection = SweetlifeConnection
    Left = 233
    Top = 91
  end
  object taProduct: TSQLDataSet
    CommandText = 'PRODUCT'
    CommandType = ctTable
    DbxCommandType = 'Dbx.Table'
    MaxBlobSize = -1
    Params = <>
    SQLConnection = SweetlifeConnection
    Left = 138
    Top = 87
  end
  object dspProducts: TDataSetProvider
    DataSet = taProduct
    Left = 136
    Top = 152
  end
  object dspOrderList: TDataSetProvider
    DataSet = taOrderList
    Left = 224
    Top = 152
  end
end
