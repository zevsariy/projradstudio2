//---------------------------------------------------------------------------

#ifndef dmuDatabaseH
#define dmuDatabaseH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <DataSnap.DSServer.hpp>
#include <Datasnap.DSProviderDataModuleAdapter.hpp>
#include <Data.DB.hpp>
#include <Data.DBXFirebird.hpp>
#include <Data.FMTBcd.hpp>
#include <Data.SqlExpr.hpp>
#include <Datasnap.Provider.hpp>
//---------------------------------------------------------------------------
class TdmDatabase : public TDSServerModule
{
__published:	// IDE-managed Components
	TSQLConnection *SweetlifeConnection;
	TSQLStoredProc *Order_list_insProc;
	TSQLDataSet *taOrderList;
	TSQLDataSet *taProduct;
	TDataSetProvider *dspProducts;
	TDataSetProvider *dspOrderList;
	void __fastcall SweetlifeConnectionBeforeConnect(TObject *Sender);
	void __fastcall SweetlifeConnectionAfterConnect(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TdmDatabase(TComponent* Owner);
	System::UnicodeString EchoString(System::UnicodeString value);
	System::UnicodeString  ReverseString(System::UnicodeString value);
	void NewOrder(int aProductID, double aDate, UnicodeString aFIO, UnicodeString aPhone, UnicodeString aNotes);
};
//---------------------------------------------------------------------------
extern PACKAGE TdmDatabase *dmDatabase;
//---------------------------------------------------------------------------
#endif

