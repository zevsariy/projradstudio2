//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "dmuDatabase.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
//---------------------------------------------------------------------------
__fastcall TdmDatabase::TdmDatabase(TComponent* Owner)
	: TDSServerModule(Owner)
{
}
//----------------------------------------------------------------------------
System::UnicodeString TdmDatabase::EchoString(System::UnicodeString value)
{
    return value;
}
//----------------------------------------------------------------------------
System::UnicodeString TdmDatabase::ReverseString(System::UnicodeString value)
{
    return ::ReverseString(value);
}
//----------------------------------------------------------------------------
void TdmDatabase::NewOrder(int aProductID, double aDate, UnicodeString aFIO,
UnicodeString aPhone, UnicodeString aNotes)
{
	 Order_list_insProc-> Params->ParamByName("PRODUCT_ID")->Value = aProductID;
	 Order_list_insProc-> Params->ParamByName("DATE")->Value = aDate;
	 Order_list_insProc-> Params->ParamByName("FIO")->Value = aFIO;
	 Order_list_insProc-> Params->ParamByName("PHONE")->Value = aPhone;
	 Order_list_insProc-> Params->ParamByName("NOTES")->Value = aNotes;
	 Order_list_insProc->ExecProc();
}
void __fastcall TdmDatabase::SweetlifeConnectionBeforeConnect(TObject *Sender)
{
	SweetlifeConnection->Params->Values["DataBase"] = "..\\..\\..\\SWEETLIFE.FDB";
}
//---------------------------------------------------------------------------
void __fastcall TdmDatabase::SweetlifeConnectionAfterConnect(TObject *Sender)
{
	taProduct->Open();
	taOrderList->Open();
}
//---------------------------------------------------------------------------
