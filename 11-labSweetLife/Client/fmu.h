//---------------------------------------------------------------------------

#ifndef fmuH
#define fmuH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.DateTimeCtrls.hpp>
#include <FMX.Edit.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.ListBox.hpp>
#include <FMX.Objects.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TTabControl *tc;
	TTabItem *tiMain;
	TTabItem *tiProducts;
	TTabItem *tiProduct;
	TTabItem *tiNewOrder;
	TTabItem *tiOrderList;
	TButton *buMainCooker;
	TButton *buMainDeliver;
	TButton *buMainOrderList;
	TButton *buMenuProducts;
	TLabel *laMainTitle;
	TImage *imMain;
	TToolBar *ToolBar1;
	TButton *Button5;
	TLabel *Label2;
	TListBox *ListBox1;
	TListBoxItem *ListBoxItem1;
	TToolBar *ToolBar2;
	TButton *Button6;
	TButton *Button7;
	TLabel *Label3;
	TLabel *Label4;
	TLabel *Label5;
	TLabel *Label6;
	TImage *Image2;
	TText *Text1;
	TToolBar *ToolBar3;
	TButton *Button8;
	TButton *buNewOrderSend;
	TLabel *Label7;
	TLabel *laNewOrderProductName;
	TLabel *laNewOrderDate;
	TLabel *laNewOrderFIO;
	TLabel *laNewOrderPhone;
	TLabel *laNewOrderNotes;
	TEdit *edNewOrderProductName;
	TEdit *edNewOrderFIO;
	TEdit *edNewOrderPhone;
	TEdit *edNewOrderNotes;
	TDateEdit *edNewOrderDate;
	TToolBar *ToolBar4;
	TButton *Button10;
	TButton *Button11;
	TLabel *Label13;
	TListBox *ListBox2;
	TListBoxItem *ListBoxItem2;
	void __fastcall buMainOrderListClick(TObject *Sender);
	void __fastcall buMenuProductsClick(TObject *Sender);
	void __fastcall tcChange(TObject *Sender);
	void __fastcall buNewOrderSendClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
