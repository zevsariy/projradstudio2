// 
// Created by the DataSnap proxy generator.
// 23.05.2017 13:27:06
// 

#include "uProxy.h"

void __fastcall TdmDatabaseClient::SweetlifeConnectionBeforeConnect(TJSONObject* Sender)
{
  if (FSweetlifeConnectionBeforeConnectCommand == NULL)
  {
    FSweetlifeConnectionBeforeConnectCommand = FDBXConnection->CreateCommand();
    FSweetlifeConnectionBeforeConnectCommand->CommandType = TDBXCommandTypes_DSServerMethod;
    FSweetlifeConnectionBeforeConnectCommand->Text = "TdmDatabase.SweetlifeConnectionBeforeConnect";
    FSweetlifeConnectionBeforeConnectCommand->Prepare();
  }
  FSweetlifeConnectionBeforeConnectCommand->Parameters->Parameter[0]->Value->SetJSONValue(Sender, FInstanceOwner);
  FSweetlifeConnectionBeforeConnectCommand->ExecuteUpdate();
}

void __fastcall TdmDatabaseClient::SweetlifeConnectionAfterConnect(TJSONObject* Sender)
{
  if (FSweetlifeConnectionAfterConnectCommand == NULL)
  {
    FSweetlifeConnectionAfterConnectCommand = FDBXConnection->CreateCommand();
    FSweetlifeConnectionAfterConnectCommand->CommandType = TDBXCommandTypes_DSServerMethod;
    FSweetlifeConnectionAfterConnectCommand->Text = "TdmDatabase.SweetlifeConnectionAfterConnect";
    FSweetlifeConnectionAfterConnectCommand->Prepare();
  }
  FSweetlifeConnectionAfterConnectCommand->Parameters->Parameter[0]->Value->SetJSONValue(Sender, FInstanceOwner);
  FSweetlifeConnectionAfterConnectCommand->ExecuteUpdate();
}

System::UnicodeString __fastcall TdmDatabaseClient::EchoString(System::UnicodeString value)
{
  if (FEchoStringCommand == NULL)
  {
    FEchoStringCommand = FDBXConnection->CreateCommand();
    FEchoStringCommand->CommandType = TDBXCommandTypes_DSServerMethod;
    FEchoStringCommand->Text = "TdmDatabase.EchoString";
    FEchoStringCommand->Prepare();
  }
  FEchoStringCommand->Parameters->Parameter[0]->Value->SetWideString(value);
  FEchoStringCommand->ExecuteUpdate();
  System::UnicodeString result = FEchoStringCommand->Parameters->Parameter[1]->Value->GetWideString();
  return result;
}

System::UnicodeString __fastcall TdmDatabaseClient::ReverseString(System::UnicodeString value)
{
  if (FReverseStringCommand == NULL)
  {
    FReverseStringCommand = FDBXConnection->CreateCommand();
    FReverseStringCommand->CommandType = TDBXCommandTypes_DSServerMethod;
    FReverseStringCommand->Text = "TdmDatabase.ReverseString";
    FReverseStringCommand->Prepare();
  }
  FReverseStringCommand->Parameters->Parameter[0]->Value->SetWideString(value);
  FReverseStringCommand->ExecuteUpdate();
  System::UnicodeString result = FReverseStringCommand->Parameters->Parameter[1]->Value->GetWideString();
  return result;
}

void __fastcall TdmDatabaseClient::NewOrder(int aProductID, double aDate, System::UnicodeString aFIO, System::UnicodeString aPhone, System::UnicodeString aNotes)
{
  if (FNewOrderCommand == NULL)
  {
    FNewOrderCommand = FDBXConnection->CreateCommand();
    FNewOrderCommand->CommandType = TDBXCommandTypes_DSServerMethod;
    FNewOrderCommand->Text = "TdmDatabase.NewOrder";
    FNewOrderCommand->Prepare();
  }
  FNewOrderCommand->Parameters->Parameter[0]->Value->SetInt32(aProductID);
  FNewOrderCommand->Parameters->Parameter[1]->Value->SetDouble(aDate);
  FNewOrderCommand->Parameters->Parameter[2]->Value->SetWideString(aFIO);
  FNewOrderCommand->Parameters->Parameter[3]->Value->SetWideString(aPhone);
  FNewOrderCommand->Parameters->Parameter[4]->Value->SetWideString(aNotes);
  FNewOrderCommand->ExecuteUpdate();
}


__fastcall  TdmDatabaseClient::TdmDatabaseClient(TDBXConnection *ADBXConnection)
{
  if (ADBXConnection == NULL)
    throw EInvalidOperation("Connection cannot be nil.  Make sure the connection has been opened.");
  FDBXConnection = ADBXConnection;
  FInstanceOwner = True;
}


__fastcall  TdmDatabaseClient::TdmDatabaseClient(TDBXConnection *ADBXConnection, bool AInstanceOwner)
{
  if (ADBXConnection == NULL) 
    throw EInvalidOperation("Connection cannot be nil.  Make sure the connection has been opened.");
  FDBXConnection = ADBXConnection;
  FInstanceOwner = AInstanceOwner;
}


__fastcall  TdmDatabaseClient::~TdmDatabaseClient()
{
  delete FSweetlifeConnectionBeforeConnectCommand;
  delete FSweetlifeConnectionAfterConnectCommand;
  delete FEchoStringCommand;
  delete FReverseStringCommand;
  delete FNewOrderCommand;
}

