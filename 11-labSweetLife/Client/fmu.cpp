//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "fmu.h"
#include "dmu.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buMainOrderListClick(TObject *Sender)
{
    tc->ActiveTab = tiOrderList;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buMenuProductsClick(TObject *Sender)
{
	tc->ActiveTab = tiProducts;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::tcChange(TObject *Sender)
{
	if(tc->ActiveTab == tiNewOrder)
	{
		edNewOrderProductName->Text = dm->cdsProductNAME->AsString;
		edNewOrderDate->Date = Now();
		edNewOrderNotes->Lines->Clear();
    }
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buNewOrderSendClick(TObject *Sender)
{
	dm->dmDatabaseClient->NewOrder(
	  dm->cdsProductID->Value,
	  edNewOrderDate->Date,
	  edNewOrderFIO->Text,
	  edNewOrderPhone->Text,
	  edNewOrderNotes->Text
	);
	dm->cdsOrderList->Refresh();
	ShowMessage(L"����� ���������");
    tc->GotoVisibleTab(tiProducts->Index);
}
//---------------------------------------------------------------------------

