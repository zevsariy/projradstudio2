object dm: Tdm
  OldCreateOrder = False
  Height = 271
  Width = 415
  object SQLConnection1: TSQLConnection
    DriverName = 'DataSnap'
    LoginPrompt = False
    Params.Strings = (
      'DriverUnit=Data.DBXDataSnap'
      'HostName=localhost'
      
        'DriverAssemblyLoader=Borland.Data.TDBXClientDriverLoader,Borland' +
        '.Data.DbxClientDriver,Version=24.0.0.0,Culture=neutral,PublicKey' +
        'Token=91d62ebb5b0d1b1b'
      'Port=211'
      'CommunicationProtocol=tcp/ip'
      'DatasnapContext=datasnap/'
      'Filters={}')
    Connected = True
    Left = 48
    Top = 40
  end
  object dsp: TDSProviderConnection
    ServerClassName = 'TdmDatabase'
    Connected = True
    SQLConnection = SQLConnection1
    Left = 200
    Top = 56
  end
  object ClientDataSet1: TClientDataSet
    Aggregates = <>
    Params = <>
    RemoteServer = dsp
    Left = 192
    Top = 120
  end
  object ClientDataSet2: TClientDataSet
    Aggregates = <>
    Params = <>
    RemoteServer = dsp
    Left = 96
    Top = 136
  end
end
