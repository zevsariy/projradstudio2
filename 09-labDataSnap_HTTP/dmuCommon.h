//---------------------------------------------------------------------------

#ifndef dmuCommonH
#define dmuCommonH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <DataSnap.DSServer.hpp>
#include <Datasnap.DSProviderDataModuleAdapter.hpp>
#include <ComObj.hpp>
//---------------------------------------------------------------------------
class TdmCommon : public TDSServerModule
{
__published:	// IDE-managed Components
private:	// User declarations
	System::UnicodeString GUID;
public:		// User declarations
    System::UnicodeString GetID();
	__fastcall TdmCommon(TComponent* Owner);
	System::UnicodeString EchoString(System::UnicodeString value);
	System::UnicodeString  ReverseString(System::UnicodeString value);
};
//---------------------------------------------------------------------------
extern PACKAGE TdmCommon *dmCommon;
//---------------------------------------------------------------------------
#endif

