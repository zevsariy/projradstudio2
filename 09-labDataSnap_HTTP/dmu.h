//----------------------------------------------------------------------------

#ifndef dmuH
#define dmuH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <DataSnap.DSCommonServer.hpp>
#include <DataSnap.DSServer.hpp>
#include <DataSnap.DSTCPServerTransport.hpp>
#include <DataSnap.DSHTTP.hpp>
#include <DataSnap.DSAuth.hpp>
#include <IPPeerServer.hpp>
//----------------------------------------------------------------------------
class Tdm : public TDataModule
{
__published:	// IDE-managed Components
	TDSServer *DSServer1;
	TDSTCPServerTransport *DSTCPServerTransport1;
	TDSHTTPService *DSHTTPService1;
	TDSAuthenticationManager *DSAuthenticationManager1;
	TDSServerClass *DSServerClass_Server;
	TDSServerClass *DSServerClass_Session;
	TDSServerClass *DSServerClass_Invocation;
	void __fastcall DSServerClass_ServerGetClass(TDSServerClass *DSServerClass, TPersistentClass &PersistentClass);
	void __fastcall DSAuthenticationManager1UserAuthenticate(TObject *Sender, const UnicodeString Protocol,
          const UnicodeString Context, const UnicodeString User,
          const UnicodeString Password, bool &valid,  TStrings *UserRoles);
	void __fastcall DSServerClass_SessionGetClass(TDSServerClass *DSServerClass, TPersistentClass &PersistentClass);
	void __fastcall DSServerClass_InvocationGetClass(TDSServerClass *DSServerClass,
          TPersistentClass &PersistentClass);

private:

public:		// User declarations
	__fastcall Tdm(TComponent* Owner);
};
//----------------------------------------------------------------------------
extern PACKAGE Tdm *dm;
//----------------------------------------------------------------------------
#endif

