//----------------------------------------------------------------------------

#ifndef dmuH
#define dmuH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include "uProxy.h"
#include <Data.DB.hpp>
#include <Data.DBXCommon.hpp>
#include <Data.DBXDataSnap.hpp>
#include <Data.SqlExpr.hpp>
#include <IPPeerClient.hpp>
//----------------------------------------------------------------------------
class Tdm : public TDataModule
{
__published:	// IDE-managed Components
	TSQLConnection *SQLConnection1;
private:	// User declarations
	bool FInstanceOwner;
	TdmServerClient* FdmServerClient;
	TdmSessionClient* FdmSessionClient;
	TdmInvocationClient* FdmInvocationClient;
	TdmServerClient* GetdmServerClient(void);
	TdmSessionClient* GetdmSessionClient(void);
	TdmInvocationClient* GetdmInvocationClient(void);
public:		// User declarations
	__fastcall Tdm(TComponent* Owner);
	__fastcall ~Tdm();
	__property bool InstanceOwner = {read=FInstanceOwner, write=FInstanceOwner};
	__property TdmServerClient* dmServerClient = {read=GetdmServerClient, write=FdmServerClient};
	__property TdmSessionClient* dmSessionClient = {read=GetdmSessionClient, write=FdmSessionClient};
	__property TdmInvocationClient* dmInvocationClient = {read=GetdmInvocationClient, write=FdmInvocationClient};
};
//---------------------------------------------------------------------------
extern PACKAGE Tdm *dm;
//---------------------------------------------------------------------------
#endif
