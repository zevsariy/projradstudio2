#ifndef uProxyH
#define uProxyH

#include "Data.DBXCommon.hpp"
#include "System.Classes.hpp"
#include "System.SysUtils.hpp"
#include "Data.DB.hpp"
#include "Data.SqlExpr.hpp"
#include "Data.DBXDBReaders.hpp"
#include "Data.DBXCDSReaders.hpp"

  class TdmServerClient : public TObject
  {
  private:
    TDBXConnection *FDBXConnection;
    bool FInstanceOwner;
    TDBXCommand *FGetIDCommand;
    TDBXCommand *FEchoStringCommand;
    TDBXCommand *FReverseStringCommand;
  public:
    __fastcall TdmServerClient(TDBXConnection *ADBXConnection);
    __fastcall TdmServerClient(TDBXConnection *ADBXConnection, bool AInstanceOwner);
    __fastcall ~TdmServerClient();
    System::UnicodeString __fastcall GetID();
    System::UnicodeString __fastcall EchoString(System::UnicodeString value);
    System::UnicodeString __fastcall ReverseString(System::UnicodeString value);
  };

  class TdmSessionClient : public TObject
  {
  private:
    TDBXConnection *FDBXConnection;
    bool FInstanceOwner;
    TDBXCommand *FGetIDCommand;
    TDBXCommand *FEchoStringCommand;
    TDBXCommand *FReverseStringCommand;
  public:
    __fastcall TdmSessionClient(TDBXConnection *ADBXConnection);
    __fastcall TdmSessionClient(TDBXConnection *ADBXConnection, bool AInstanceOwner);
    __fastcall ~TdmSessionClient();
    System::UnicodeString __fastcall GetID();
    System::UnicodeString __fastcall EchoString(System::UnicodeString value);
    System::UnicodeString __fastcall ReverseString(System::UnicodeString value);
  };

  class TdmInvocationClient : public TObject
  {
  private:
    TDBXConnection *FDBXConnection;
    bool FInstanceOwner;
    TDBXCommand *FGetIDCommand;
    TDBXCommand *FEchoStringCommand;
    TDBXCommand *FReverseStringCommand;
  public:
    __fastcall TdmInvocationClient(TDBXConnection *ADBXConnection);
    __fastcall TdmInvocationClient(TDBXConnection *ADBXConnection, bool AInstanceOwner);
    __fastcall ~TdmInvocationClient();
    System::UnicodeString __fastcall GetID();
    System::UnicodeString __fastcall EchoString(System::UnicodeString value);
    System::UnicodeString __fastcall ReverseString(System::UnicodeString value);
  };

#endif
