object dm: Tdm
  OldCreateOrder = False
  Height = 271
  Width = 415
  object DSServer1: TDSServer
    Left = 96
    Top = 11
  end
  object DSTCPServerTransport1: TDSTCPServerTransport
    Server = DSServer1
    Filters = <>
    AuthenticationManager = DSAuthenticationManager1
    Left = 96
    Top = 73
  end
  object DSHTTPService1: TDSHTTPService
    HttpPort = 8080
    Server = DSServer1
    Filters = <>
    AuthenticationManager = DSAuthenticationManager1
    Left = 96
    Top = 135
  end
  object DSAuthenticationManager1: TDSAuthenticationManager
    OnUserAuthenticate = DSAuthenticationManager1UserAuthenticate
    Roles = <
      item
        AuthorizedRoles.Strings = (
          'admins')
        ApplyTo.Strings = (
          'TdmSession.EchoString')
      end>
    Left = 96
    Top = 197
  end
  object DSServerClass_Server: TDSServerClass
    OnGetClass = DSServerClass_ServerGetClass
    Server = DSServer1
    LifeCycle = 'Server'
    Left = 200
    Top = 11
  end
  object DSServerClass_Session: TDSServerClass
    OnGetClass = DSServerClass_SessionGetClass
    Server = DSServer1
    Left = 240
    Top = 83
  end
  object DSServerClass_Invocation: TDSServerClass
    OnGetClass = DSServerClass_InvocationGetClass
    Server = DSServer1
    LifeCycle = 'Invocation'
    Left = 216
    Top = 155
  end
end
