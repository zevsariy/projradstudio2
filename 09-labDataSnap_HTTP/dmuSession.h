//---------------------------------------------------------------------------

#ifndef dmuSessionH
#define dmuSessionH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include "dmuCommon.h"
//---------------------------------------------------------------------------
class TdmSession : public TdmCommon
{
__published:	// IDE-managed Components
private:	// User declarations
public:		// User declarations
	__fastcall TdmSession(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TdmSession *dmSession;
//---------------------------------------------------------------------------
#endif
