//---------------------------------------------------------------------------

#ifndef dmuServerH
#define dmuServerH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include "dmuCommon.h"
//---------------------------------------------------------------------------
class TdmServer : public TdmCommon
{
__published:	// IDE-managed Components
private:	// User declarations
public:		// User declarations
	__fastcall TdmServer(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TdmServer *dmServer;
//---------------------------------------------------------------------------
#endif
