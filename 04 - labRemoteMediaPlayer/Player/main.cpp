//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buOpenClick(TObject *Sender)
{
	od->Filter = TMediaCodecManager::GetFilterString();
	if (od->Execute()) {
		mp->FileName = od->FileName;
//      laDuration->Text = " ____ //TODO
	}
}
//---------------------------------------------------------------------------


void __fastcall Tfm::acPlayPauseExecute(TObject *Sender)
{
    acMediaPlayerPlayPause->Execute();
}
//---------------------------------------------------------------------------

void __fastcall Tfm::acStopExecute(TObject *Sender)
{
    acMediaPlayerStop->Execute();
}
//---------------------------------------------------------------------------

void __fastcall Tfm::tbVolumeChange(TObject *Sender)
{
	ttp->Resources->FindByName("Volume")->Value = tbVolume->Value;
    ttp->Resources->FindByName("VolumeMax")->Value = tbVolume->Max;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::tbTimeChange(TObject *Sender)
{
	ttp->Resources->FindByName("Time")->Value = tbTime->Value;
	ttp->Resources->FindByName("TimeMax")->Value = tbTime->Max;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::ttpResourceReceived(TObject * const Sender, TRemoteResource * const AResource)

{
    meLog->Lines->Add(AResource->Hint + " = " + AResource->Value.AsString);
}
//--------------------------------------------
