//---------------------------------------------------------------------------

#ifndef mainH
#define mainH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Memo.hpp>
#include <FMX.ScrollBox.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <FMX.Dialogs.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.Media.hpp>
#include <IPPeerClient.hpp>
#include <IPPeerServer.hpp>
#include <System.Tether.AppProfile.hpp>
#include <System.Tether.Manager.hpp>
#include <FMX.ActnList.hpp>
#include <FMX.StdActns.hpp>
#include <System.Actions.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TToolBar *tb;
	TButton *buOpen;
	TButton *buPlay;
	TButton *buStop;
	TButton *buAbout;
	TButton *buCLear;
	TMemo *meChat;
	TMediaPlayer *mp;
	TMediaPlayerControl *mpc;
	TTrackBar *tbTime;
	TLabel *Label1;
	TLabel *Label2;
	TLayout *ly1;
	TLayout *ly2;
	TLabel *Label3;
	TLabel *Label4;
	TTrackBar *tbVolume;
	TOpenDialog *op;
	TTetheringManager *ttm;
	TTetheringAppProfile *ttp;
	TActionList *ac;
	TMediaPlayerStop *acMediaPlayerStop;
	TMediaPlayerPlayPause *acMediaPlayerPlayPause;
	TMediaPlayerCurrentTime *acMediaPlayerCurrentTime;
	TMediaPlayerVolume *acMediaPlayerVolume;
	TAction *acClear;
	TAction *acPlayPause;
	TAction *acStop;
	void __fastcall buOpenClick(TObject *Sender);
	void __fastcall acPlayPauseExecute(TObject *Sender);
	void __fastcall acStopExecute(TObject *Sender);
	void __fastcall tbVolumeChange(TObject *Sender);
	void __fastcall tbTimeChange(TObject *Sender);
	void __fastcall ttpResourceReceived(TObject * const Sender, TRemoteResource * const AResource);
private:	// User declarations
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
