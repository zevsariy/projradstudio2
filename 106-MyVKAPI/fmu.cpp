//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "fmu.h"
#include "dmu.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buDoItClick(TObject *Sender)
{
	TJSONValue *jValue;
	String UserId = edID->Text;
	dm->Client->BaseURL = "https://api.vk.com/method/users.get?user_ids=" + UserId + "&fields=bdate,city,personal&v=5.65&access_token=ca56b71473b5e43fee80b1fd2c270cceb3c8030cfb423d2f91f6081338320e5ea79ca525a11f9adc76f9a";
	dm->Req->Execute();
	ShowMessage(dm->Resp->JSONText);

	TJSONValue* JSONAPI = TJSONObject::ParseJSONValue(TEncoding::UTF8->GetBytes(dm->Resp->JSONText),0);
	String id = JSONAPI->GetValue<UnicodeString>("response[0].id");
	ShowMessage(JSONAPI->GetValue<UnicodeString>("response[0].first_name"));

	laFirstName->Text = JSONAPI->GetValue<UnicodeString>("response[0].first_name");
	laLastName->Text = JSONAPI->GetValue<UnicodeString>("response[0].last_name");
	laBDate->Text = JSONAPI->GetValue<UnicodeString>("response[0].bdate");
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buCreateClick(TObject *Sender)
{
	 for(int i=0; i < 100; i++)
	 {
		TListViewItem* lvItem;
		lvItem = lvMain->Items->Add();
		lvItem->Text = "Item - " + IntToStr(i);
		lvItem->Tag = i;
     }
}
//---------------------------------------------------------------------------
void __fastcall Tfm::lvMainItemClick(TObject * const Sender, TListViewItem * const AItem)

{
    edID->Text = IntToStr(AItem->Tag);
}
//---------------------------------------------------------------------------


