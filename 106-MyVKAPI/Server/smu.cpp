//----------------------------------------------------------------------------
#include <fmx.h>
#pragma hdrstop
#include <tchar.h>
#include <stdio.h>
#include <memory>
#include <string>
#include "dmu.h"

#include "smu.h"
#include "fmu.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma classgroup "FMX.Controls.TControl"
#pragma resource "*.dfm"

//---------------------------------------------------------------------------
char const *sPortInUse = "- Error: Port already in use\n";
char const *sPortSet = "- Port set to %d\n";
char const *sPortNotSet = "- Port could not be set\n";
char const *sServerRunning = "- The Server is already running\n";
char const *sStartingServer = "- Starting Server\n";
char const *sServerIsRunning = "- Server Running\n";
char const *sStoppingServer = "- Stopping Server\n";
char const *sServerStopped = "- Server Stopped\n";
char const *sServerNotRunning = "- The Server is not running\n";
char const *sInvalidCommand = "- Error: Invalid Command\n";
char const *sIndyVersion = "- Indy Version: ";
char const *sActive = "- Active: ";
char const *sTCPIPPort = "- TCP/IP Port: ";
char const *sHTTPPort = "- HTTP Port: ";
char const *sHTTPSPort = "- HTTPS Port: ";
char const *sSessionID = "- Session ID CookieName: ";
char const *sCommands = "Enter a Command: \n"
	"   - \"start\" to start the server\n"
	"   - \"stop\" to stop the server\n"
	"   - \"set port -t\" to change the TCP/IP default port\n"
	"   - \"set port -h\" to change the HTTP default port\n"
	"   - \"set port -s\" to change the HTTPS default port\n"
	"   - \"status\" for Server status\n"
	"   - \"help\" to show commands\n"
	"   - \"exit\" to close the application\n";
char const *sArrow = "->";
char const *sCommandStart = "start";
char const *sCommandStop = "stop";
char const *sCommandStatus = "status";
char const *sCommandHelp = "help";
char const *sCommandSetTCPIPPort = "set port -t";
char const *sCommandSetHTTPPort = "set port -h";
char const *sCommandSetHTTPSPort = "set port -s";
char const *sCommandExit = "exit";

typedef enum {TCPIP, HTTP, HTTPS} TDSProtocol;
//---------------------------------------------------------------------------

TServer *ServerContainer1;

//---------------------------------------------------------------------------
__fastcall TServer::TServer(TComponent* Owner)
	: TDataModule(Owner)
{
}
//----------------------------------------------------------------------------
void __fastcall TServer::DSServerClass1GetClass(TDSServerClass *DSServerClass,
          TPersistentClass &PersistentClass)
{
	PersistentClass =  __classid(TServerMethods1);
}
//----------------------------------------------------------------------------

void writeCommands()
{
	printf(sCommands);
	printf(sArrow);
}

void startServer(std::unique_ptr<TServer>const& module)
{
	if (!module->DSServer->Started) {
		try {
			module->DSServer->Start();
		} catch (Exception &exception) {		
			printf(sPortInUse);		
		}
		
		if (module->DSServer->Started) printf(sStartingServer);
	}
	else {
		printf(sServerRunning);
	}

	printf(sArrow);
}

void stopServer(std::unique_ptr<TServer>const& module)
{
	if (module->DSServer->Started) {
		printf(sStoppingServer);
		module->DSServer->Stop();
		printf(sServerStopped);
	}
	else {
		printf(sServerNotRunning);
	}

	printf(sArrow);
}

void setPort(std::unique_ptr<TServer>const& module, int port, TDSProtocol protocol)
{
	if (!module->DSServer->Started) {
		switch(protocol) {	
		case TCPIP:
			module->DSTCPServerTransport1->Port = port;
			break;
		
		
					
		}	
		printf(sPortSet, port);
	}
	else {
		printf(sServerRunning);
	}
	printf(sArrow);
}

void writeStatus(std::unique_ptr<TServer>const& module)
{
	printf("%s %s\n", sActive, (module->DSServer->Started) ? "true" : "false");
	printf("%s %d\n", sTCPIPPort, module->DSTCPServerTransport1->Port);
    
	printf(sArrow);
}

//---------------------------------------------------------------------------

void __fastcall TServer::VkapiConnectionAfterConnect(TObject *Sender)
{
	fm->laStatus->Text = VkapiConnection->Connected ? "true" : "false";
}
//---------------------------------------------------------------------------
