//----------------------------------------------------------------------------

#ifndef smuH
#define smuH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <DataSnap.DSCommonServer.hpp>
#include <DataSnap.DSServer.hpp>
#include <DataSnap.DSTCPServerTransport.hpp>
#include <DataSnap.DSAuth.hpp>
#include <IPPeerServer.hpp>
#include <Data.DB.hpp>
#include <Data.DBXFirebird.hpp>
#include <Data.FMTBcd.hpp>
#include <Data.SqlExpr.hpp>
#include <Datasnap.Provider.hpp>
//----------------------------------------------------------------------------
class TServer : public TDataModule
{
__published:	// IDE-managed Components
	TDSServer *DSServer;
	TDSTCPServerTransport *DSTCPServerTransport1;
	TDSServerClass *DSServerClass1;
	TSQLConnection *VkapiConnection;
	TSQLDataSet *UsersTable;
	TDataSetProvider *dspUsers;
	void __fastcall DSServerClass1GetClass(TDSServerClass *DSServerClass, TPersistentClass &PersistentClass);
	void __fastcall VkapiConnectionAfterConnect(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TServer(TComponent* Owner);
};
//----------------------------------------------------------------------------
extern PACKAGE TServer *Server;
//----------------------------------------------------------------------------
#endif

