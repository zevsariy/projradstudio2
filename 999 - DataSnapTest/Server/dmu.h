//---------------------------------------------------------------------------

#ifndef dmuH
#define dmuH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <DataSnap.DSServer.hpp>
#include <Datasnap.DSProviderDataModuleAdapter.hpp>
#include <Data.DB.hpp>
#include <Data.DBXFirebird.hpp>
#include <Data.FMTBcd.hpp>
#include <Data.SqlExpr.hpp>
#include <Datasnap.Provider.hpp>
//---------------------------------------------------------------------------
class Tdm : public TDSServerModule
{
__published:	// IDE-managed Components
	TSQLConnection *BeautysaloonConnection;
	TSQLDataSet *CategoryTable;
	TSQLDataSet *ServiceTable;
	TDataSetProvider *dspCategory;
	TDataSetProvider *dspService;
private:	// User declarations
public:		// User declarations
	__fastcall Tdm(TComponent* Owner);
	System::UnicodeString EchoString(System::UnicodeString value);
	System::UnicodeString  ReverseString(System::UnicodeString value);
};
//---------------------------------------------------------------------------
extern PACKAGE Tdm *dm;
//---------------------------------------------------------------------------
#endif

