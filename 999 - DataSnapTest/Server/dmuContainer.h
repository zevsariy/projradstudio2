//----------------------------------------------------------------------------

#ifndef dmuContainerH
#define dmuContainerH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <DataSnap.DSCommonServer.hpp>
#include <DataSnap.DSServer.hpp>
#include <DataSnap.DSTCPServerTransport.hpp>
#include <DataSnap.DSAuth.hpp>
#include <IPPeerServer.hpp>
//----------------------------------------------------------------------------
class TdmContainer : public TDataModule
{
__published:	// IDE-managed Components
	TDSServer *DSServer1;
	TDSTCPServerTransport *DSTCPServerTransport1;
	TDSServerClass *DSServerClass1;
	void __fastcall DSServerClass1GetClass(TDSServerClass *DSServerClass, TPersistentClass &PersistentClass);
private:	// User declarations
public:		// User declarations
	__fastcall TdmContainer(TComponent* Owner);
};
//----------------------------------------------------------------------------
extern PACKAGE TdmContainer *dmContainer;
//----------------------------------------------------------------------------
#endif

