#ifndef Unit1H
#define Unit1H

#include "Data.DBXCommon.hpp"
#include "System.Classes.hpp"
#include "System.SysUtils.hpp"
#include "Data.DB.hpp"
#include "Data.SqlExpr.hpp"
#include "Data.DBXDBReaders.hpp"
#include "Data.DBXCDSReaders.hpp"

  class TdmServerClient : public TObject
  {
  private:
    TDBXConnection *FDBXConnection;
    bool FInstanceOwner;
    TDBXCommand *FEchoStringCommand;
    TDBXCommand *FReverseStringCommand;
    TDBXCommand *FAddNewCommand;
  public:
    __fastcall TdmServerClient(TDBXConnection *ADBXConnection);
    __fastcall TdmServerClient(TDBXConnection *ADBXConnection, bool AInstanceOwner);
    __fastcall ~TdmServerClient();
    System::UnicodeString __fastcall EchoString(System::UnicodeString value);
    System::UnicodeString __fastcall ReverseString(System::UnicodeString value);
    void __fastcall AddNew(System::UnicodeString Name, int VKID);
  };

#endif

