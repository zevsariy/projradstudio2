//
// Created by the DataSnap proxy generator.
// 18.06.2017 15:22:17
//

#include "uProxy.h"

System::UnicodeString __fastcall TdmServerClient::EchoString(System::UnicodeString value)
{
  if (FEchoStringCommand == NULL)
  {
    FEchoStringCommand = FDBXConnection->CreateCommand();
    FEchoStringCommand->CommandType = TDBXCommandTypes_DSServerMethod;
    FEchoStringCommand->Text = "TdmServer.EchoString";
    FEchoStringCommand->Prepare();
  }
  FEchoStringCommand->Parameters->Parameter[0]->Value->SetWideString(value);
  FEchoStringCommand->ExecuteUpdate();
  System::UnicodeString result = FEchoStringCommand->Parameters->Parameter[1]->Value->GetWideString();
  return result;
}

System::UnicodeString __fastcall TdmServerClient::ReverseString(System::UnicodeString value)
{
  if (FReverseStringCommand == NULL)
  {
    FReverseStringCommand = FDBXConnection->CreateCommand();
    FReverseStringCommand->CommandType = TDBXCommandTypes_DSServerMethod;
    FReverseStringCommand->Text = "TdmServer.ReverseString";
    FReverseStringCommand->Prepare();
  }
  FReverseStringCommand->Parameters->Parameter[0]->Value->SetWideString(value);
  FReverseStringCommand->ExecuteUpdate();
  System::UnicodeString result = FReverseStringCommand->Parameters->Parameter[1]->Value->GetWideString();
  return result;
}

void __fastcall TdmServerClient::AddNew(System::UnicodeString Name, int VKID)
{
  if (FAddNewCommand == NULL)
  {
    FAddNewCommand = FDBXConnection->CreateCommand();
    FAddNewCommand->CommandType = TDBXCommandTypes_DSServerMethod;
    FAddNewCommand->Text = "TdmServer.AddNew";
    FAddNewCommand->Prepare();
  }
  FAddNewCommand->Parameters->Parameter[0]->Value->SetWideString(Name);
  FAddNewCommand->Parameters->Parameter[1]->Value->SetInt32(VKID);
  FAddNewCommand->ExecuteUpdate();
}


__fastcall  TdmServerClient::TdmServerClient(TDBXConnection *ADBXConnection)
{
  if (ADBXConnection == NULL)
    throw EInvalidOperation("Connection cannot be nil.  Make sure the connection has been opened.");
  FDBXConnection = ADBXConnection;
  FInstanceOwner = True;
}


__fastcall  TdmServerClient::TdmServerClient(TDBXConnection *ADBXConnection, bool AInstanceOwner)
{
  if (ADBXConnection == NULL)
    throw EInvalidOperation("Connection cannot be nil.  Make sure the connection has been opened.");
  FDBXConnection = ADBXConnection;
  FInstanceOwner = AInstanceOwner;
}


__fastcall  TdmServerClient::~TdmServerClient()
{
  delete FEchoStringCommand;
  delete FReverseStringCommand;
  delete FAddNewCommand;
}


