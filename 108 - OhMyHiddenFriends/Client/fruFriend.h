//---------------------------------------------------------------------------

#ifndef fruFriendH
#define fruFriendH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <FMX.Edit.hpp>
//---------------------------------------------------------------------------
class TFrame1 : public TFrame
{
__published:	// IDE-managed Components
	TLabel *laFirstName;
	TLabel *laLastName;
	TLabel *laBD;
	TEdit *edVKUrl;
	void __fastcall edVKUrlClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TFrame1(TComponent* Owner);
    int VKID;
};
//---------------------------------------------------------------------------
extern PACKAGE TFrame1 *Frame1;
//---------------------------------------------------------------------------
#endif
