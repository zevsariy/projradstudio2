//----------------------------------------------------------------------------

#ifndef dmuH
#define dmuH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include "uProxy.h"
#include <Data.DB.hpp>
#include <Data.DBXCommon.hpp>
#include <Data.DBXDataSnap.hpp>
#include <Data.SqlExpr.hpp>
#include <IPPeerClient.hpp>
#include <Datasnap.DBClient.hpp>
#include <Datasnap.DSConnect.hpp>
#include <Data.Bind.Components.hpp>
#include <Data.Bind.ObjectScope.hpp>
#include <REST.Client.hpp>
//----------------------------------------------------------------------------
class Tdm : public TDataModule
{
__published:	// IDE-managed Components
	TSQLConnection *SQLConnection1;
	TDSProviderConnection *DSProviderConnection1;
	TClientDataSet *cdsFriends;
	TRESTClient *restClient;
	TRESTRequest *restReq;
	TRESTResponse *restResp;
	TClientDataSet *cdsAddNew;
private:	// User declarations
	bool FInstanceOwner;
	TdmServerClient* FdmServerClient;
	TdmServerClient* GetdmServerClient(void);
public:		// User declarations
	__fastcall Tdm(TComponent* Owner);
	__fastcall ~Tdm();
	__property bool InstanceOwner = {read=FInstanceOwner, write=FInstanceOwner};
	__property TdmServerClient* dmServerClient = {read=GetdmServerClient, write=FdmServerClient};
};
//---------------------------------------------------------------------------
extern PACKAGE Tdm *dm;
//---------------------------------------------------------------------------
#endif
