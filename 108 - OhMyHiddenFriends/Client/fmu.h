//---------------------------------------------------------------------------

#ifndef fmuH
#define fmuH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.StdCtrls.hpp>
#include "fruFriend.h"
#include <Data.Bind.Components.hpp>
#include <Data.Bind.DBScope.hpp>
#include <Data.Bind.EngExt.hpp>
#include <Fmx.Bind.DBEngExt.hpp>
#include <Fmx.Bind.Editors.hpp>
#include <FMX.ListView.Adapters.Base.hpp>
#include <FMX.ListView.Appearances.hpp>
#include <FMX.ListView.hpp>
#include <FMX.ListView.Types.hpp>
#include <System.Bindings.Outputs.hpp>
#include <System.Rtti.hpp>
#include <FMX.Edit.hpp>
#include <FMX.Objects.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TTabControl *tc;
	TTabItem *tiMain;
	TTabItem *tiHiddenFriends;
	TTabItem *tiFriend;
	TTabItem *tiAddHiddenFriend;
	TToolBar *tbHiddenFriends;
	TButton *buBackHiddenFriends;
	TToolBar *tbMain;
	TToolBar *tbFriend;
	TButton *buBackFriend;
	TFrame1 *frFriend;
	TListView *lvHiddenFriends;
	TLabel *laMainTitle;
	TButton *Button1;
	TButton *buStart;
	TButton *buExit;
	TTabItem *tiAbout;
	TBindSourceDB *BindSourceDB1;
	TBindingsList *BindingsList1;
	TLinkListControlToField *LinkListControlToField1;
	TLinkPropertyToField *LinkPropertyToFieldTag;
	TToolBar *tbAddHiddenFriend;
	TButton *buBackAddFriend;
	TButton *buAddFriend;
	TToolBar *tbAbout;
	TButton *buBackAbout;
	TStyleBook *StyleBook1;
	TLabel *laAddName;
	TEdit *edAddName;
	TEdit *edAddId;
	TLabel *laAddId;
	TButton *buAddNew;
	TLabel *Label1;
	TLabel *Label2;
	TLabel *Label3;
	TLabel *Label4;
	TImage *imLogo;
	TImage *imAboutLogo;
	TText *txAbout;
	void __fastcall buMainExitClick(TObject *Sender);
	void __fastcall buBackHiddenFriendsClick(TObject *Sender);
	void __fastcall buBackFriendClick(TObject *Sender);
	void __fastcall buAddFriendClick(TObject *Sender);
	void __fastcall lvHiddenFriendsItemClick(TObject * const Sender, TListViewItem * const AItem);
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall buStartClick(TObject *Sender);
	void __fastcall Button1Click(TObject *Sender);
	void __fastcall buBackAboutClick(TObject *Sender);
	void __fastcall buAddNewClick(TObject *Sender);
	void __fastcall buBackAddFriendClick(TObject *Sender);

private:	// User declarations
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
