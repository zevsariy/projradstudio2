object dm: Tdm
  OldCreateOrder = False
  Height = 271
  Width = 415
  object SQLConnection1: TSQLConnection
    DriverName = 'DataSnap'
    LoginPrompt = False
    Params.Strings = (
      'Port=211'
      'CommunicationProtocol=tcp/ip'
      'DatasnapContext=datasnap/')
    Connected = True
    Left = 160
    Top = 56
    UniqueId = '{47EBEE48-3DCF-4FC6-8FEE-4DB52017DD92}'
  end
  object DSProviderConnection1: TDSProviderConnection
    ServerClassName = 'TdmServer'
    Connected = True
    SQLConnection = SQLConnection1
    Left = 160
    Top = 120
  end
  object cdsFriends: TClientDataSet
    Active = True
    Aggregates = <>
    Params = <>
    ProviderName = 'dspFriends'
    RemoteServer = DSProviderConnection1
    Left = 88
    Top = 168
  end
  object restClient: TRESTClient
    Params = <>
    HandleRedirects = True
    Left = 312
    Top = 72
  end
  object restReq: TRESTRequest
    Client = restClient
    Params = <>
    Response = restResp
    SynchronizedEvents = False
    Left = 312
    Top = 136
  end
  object restResp: TRESTResponse
    Left = 312
    Top = 200
  end
  object cdsAddNew: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dspAddNew'
    RemoteServer = DSProviderConnection1
    Left = 24
    Top = 168
  end
end
