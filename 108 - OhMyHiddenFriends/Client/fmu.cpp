//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "fmu.h"
#include "dmu.h"
#include "uProxy.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "fruFriend"
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buMainExitClick(TObject *Sender)
{
    Close();
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buBackHiddenFriendsClick(TObject *Sender)
{
	tc->ActiveTab = tiMain;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buBackFriendClick(TObject *Sender)
{
	tc->ActiveTab = tiHiddenFriends;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buAddFriendClick(TObject *Sender)
{
    tc->ActiveTab = tiAddHiddenFriend;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::lvHiddenFriendsItemClick(TObject * const Sender, TListViewItem * const AItem)

{
	TJSONValue *jValue;
	dm->restClient->BaseURL = "https://api.vk.com/method/users.get?user_ids=" + IntToStr(lvHiddenFriends->Tag) + "&fields=bdate,city,personal&v=5.65&access_token=ca56b71473b5e43fee80b1fd2c270cceb3c8030cfb423d2f91f6081338320e5ea79ca525a11f9adc76f9a";
	dm->restReq->Execute();

	TJSONValue* JSONAPI = TJSONObject::ParseJSONValue(TEncoding::UTF8->GetBytes(dm->restResp->JSONText),0);

	frFriend->laFirstName->Text = L"���:" + JSONAPI->GetValue<UnicodeString>("response[0].first_name");
	frFriend->laLastName->Text =  L"�������:" + JSONAPI->GetValue<UnicodeString>("response[0].last_name");
	frFriend->VKID = lvHiddenFriends->Tag;
	frFriend->edVKUrl->Text = "https://vk.com/id" + IntToStr( lvHiddenFriends->Tag);
	try
	{
		frFriend->laBD->Text =  L"���� ��������:" + JSONAPI->GetValue<UnicodeString>("response[0].bdate");
	}
	catch(...)
	{
		frFriend->laBD->Text = L"���� �������� �� �������";
		tc->ActiveTab = tiFriend;
	}
	tc->ActiveTab = tiFriend;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::FormCreate(TObject *Sender)
{
	tc->ActiveTab = tiMain;
    //dm->cdsFriends->Open();
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buStartClick(TObject *Sender)
{
	BindSourceDB1->DataSet = dm->cdsFriends;
	tc->ActiveTab = tiHiddenFriends;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::Button1Click(TObject *Sender)
{
    tc->ActiveTab = tiAbout;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buBackAboutClick(TObject *Sender)
{
	tc->ActiveTab = tiMain;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buAddNewClick(TObject *Sender)
{
	dm->dmServerClient->AddNew(edAddName->Text, StrToInt(edAddId->Text));
	ShowMessage(L"����� ������������ ������� ��������");
	tc->ActiveTab = tiHiddenFriends;
    lvHiddenFriends->Items->Clear();
	dm->cdsFriends->Close();
	dm->cdsFriends->Open();
}
//---------------------------------------------------------------------------


void __fastcall Tfm::buBackAddFriendClick(TObject *Sender)
{
    tc->ActiveTab = tiHiddenFriends;
}
//---------------------------------------------------------------------------

