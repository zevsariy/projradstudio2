//----------------------------------------------------------------------------

#pragma hdrstop
#include <stdio.h>
#include <memory>

#include "dmu.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma classgroup "FMX.Controls.TControl"
#pragma resource "*.dfm"
Tdm *dm;
//---------------------------------------------------------------------------
__fastcall Tdm::Tdm(TComponent* Owner)
	: TDataModule(Owner)
{
	FInstanceOwner = true;
}

__fastcall Tdm::~Tdm()
{
	delete FdmServerClient;
}

TdmServerClient* Tdm::GetdmServerClient(void)
{
	if (FdmServerClient == NULL)
	{
		SQLConnection1->Open();
		FdmServerClient = new TdmServerClient(SQLConnection1->DBXConnection, FInstanceOwner);
	}
	return FdmServerClient;
};

