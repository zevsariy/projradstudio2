//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "dmuServer.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
//---------------------------------------------------------------------------
__fastcall TdmServer::TdmServer(TComponent* Owner)
	: TDSServerModule(Owner)
{
}
//----------------------------------------------------------------------------
System::UnicodeString TdmServer::EchoString(System::UnicodeString value)
{
    return value;
}
//----------------------------------------------------------------------------
System::UnicodeString TdmServer::ReverseString(System::UnicodeString value)
{
    return ::ReverseString(value);
}
//----------------------------------------------------------------------------
void TdmServer::AddNew(UnicodeString Name, int VKID)
{
	Hidden_friends_insProc->ParamByName("NAME")->Value = Name;
	Hidden_friends_insProc->ParamByName("VKID")->Value = VKID;
	Hidden_friends_insProc->ExecProc();
}

