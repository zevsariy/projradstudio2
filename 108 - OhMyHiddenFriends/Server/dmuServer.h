//---------------------------------------------------------------------------

#ifndef dmuServerH
#define dmuServerH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <DataSnap.DSServer.hpp>
#include <Datasnap.DSProviderDataModuleAdapter.hpp>
#include <Data.DB.hpp>
#include <Data.DBXFirebird.hpp>
#include <Data.FMTBcd.hpp>
#include <Data.SqlExpr.hpp>
#include <Datasnap.Provider.hpp>
//---------------------------------------------------------------------------
class TdmServer : public TDSServerModule
{
__published:	// IDE-managed Components
	TSQLConnection *OhmyhiddenfriendConnection;
	TSQLDataSet *Hidden_friendsTable;
	TDataSetProvider *dspFriends;
	TSQLStoredProc *Hidden_friends_insProc;
private:	// User declarations
public:		// User declarations
	__fastcall TdmServer(TComponent* Owner);
	System::UnicodeString EchoString(System::UnicodeString value);
	System::UnicodeString  ReverseString(System::UnicodeString value);
	void TdmServer::AddNew(UnicodeString Name, int VKID);
};
//---------------------------------------------------------------------------
extern PACKAGE TdmServer *dmServer;
//---------------------------------------------------------------------------
#endif

