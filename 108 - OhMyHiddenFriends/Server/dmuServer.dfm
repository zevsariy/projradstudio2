object dmServer: TdmServer
  OldCreateOrder = False
  Height = 382
  Width = 466
  object OhmyhiddenfriendConnection: TSQLConnection
    ConnectionName = 'OHMYHIDDENFRIEND'
    DriverName = 'Firebird'
    LoginPrompt = False
    Params.Strings = (
      'DriverName=Firebird'
      'DriverUnit=Data.DBXFirebird'
      
        'DriverPackageLoader=TDBXDynalinkDriverLoader,DbxCommonDriver250.' +
        'bpl'
      
        'DriverAssemblyLoader=Borland.Data.TDBXDynalinkDriverLoader,Borla' +
        'nd.Data.DbxCommonDriver,Version=24.0.0.0,Culture=neutral,PublicK' +
        'eyToken=91d62ebb5b0d1b1b'
      
        'MetaDataPackageLoader=TDBXFirebirdMetaDataCommandFactory,DbxFire' +
        'birdDriver250.bpl'
      
        'MetaDataAssemblyLoader=Borland.Data.TDBXFirebirdMetaDataCommandF' +
        'actory,Borland.Data.DbxFirebirdDriver,Version=24.0.0.0,Culture=n' +
        'eutral,PublicKeyToken=91d62ebb5b0d1b1b'
      'LibraryName=dbxfb.dll'
      'LibraryNameOsx=libsqlfb.dylib'
      'VendorLib=fbclient.dll'
      'VendorLibWin64=fbclient.dll'
      'VendorLibOsx=/Library/Frameworks/Firebird.framework/Firebird'
      'Database=C:\OHMYHIDDENFRIEND.FDB'
      'User_Name=sysdba'
      'Password=masterkey'
      'Role=RoleName'
      'MaxBlobSize=-1'
      'LocaleCode=0000'
      'IsolationLevel=ReadCommitted'
      'SQLDialect=3'
      'CommitRetain=False'
      'WaitOnLocks=True'
      'TrimChar=False'
      'BlobSize=-1'
      'ErrorResourceFile='
      'RoleName=RoleName'
      'ServerCharSet=UTF8'
      'Trim Char=False')
    Connected = True
    Left = 175
    Top = 75
  end
  object Hidden_friendsTable: TSQLDataSet
    Active = True
    CommandText = 'HIDDEN_FRIENDS'
    CommandType = ctTable
    DbxCommandType = 'Dbx.Table'
    MaxBlobSize = -1
    Params = <>
    SQLConnection = OhmyhiddenfriendConnection
    Left = 175
    Top = 163
  end
  object dspFriends: TDataSetProvider
    DataSet = Hidden_friendsTable
    Left = 168
    Top = 264
  end
  object Hidden_friends_insProc: TSQLStoredProc
    MaxBlobSize = -1
    Params = <
      item
        DataType = ftWideString
        Precision = 255
        Name = 'NAME'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Precision = 4
        Name = 'VKID'
        ParamType = ptInput
      end>
    SQLConnection = OhmyhiddenfriendConnection
    StoredProcName = 'HIDDEN_FRIENDS_INS'
    Left = 289
    Top = 254
  end
end
