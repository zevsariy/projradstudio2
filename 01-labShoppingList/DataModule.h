//---------------------------------------------------------------------------

#ifndef DataModuleH
#define DataModuleH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Data.DB.hpp>
#include <FireDAC.Comp.Client.hpp>
#include <FireDAC.Comp.DataSet.hpp>
#include <FireDAC.DApt.Intf.hpp>
#include <FireDAC.DatS.hpp>
#include <FireDAC.Phys.Intf.hpp>
#include <FireDAC.Stan.Error.hpp>
#include <FireDAC.Stan.Intf.hpp>
#include <FireDAC.Stan.Option.hpp>
#include <FireDAC.Stan.Param.hpp>
#include <FireDAC.Stan.StorageJSON.hpp>
#include <FMX.ImgList.hpp>
#include <System.ImageList.hpp>
#include <System.IOUtils.hpp>
//---------------------------------------------------------------------------
class Tdm : public TDataModule
{
__published:	// IDE-managed Components
	TFDMemTable *taList;
	TImageList *il;
	TFDStanStorageJSONLink *FDStanStorageJSONLink1;
	TStringField *taListText;
	TMemoField *taListDetail;
	TStringField *taListHeaderText;
	TIntegerField *taListImageIndex;
	TBooleanField *taListCheckmark;
	void __fastcall DataModuleCreate(TObject *Sender);
	void __fastcall taListAfterInsert(TDataSet *DataSet);
	void __fastcall taListAfterPost(TDataSet *DataSet);
private:	// User declarations
public:		// User declarations
	__fastcall Tdm(TComponent* Owner);
	UnicodeString FFileName;
};
//---------------------------------------------------------------------------
extern PACKAGE Tdm *dm;
//---------------------------------------------------------------------------
#endif
