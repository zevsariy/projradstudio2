//---------------------------------------------------------------------------

#ifndef mainH
#define mainH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.ListView.Adapters.Base.hpp>
#include <FMX.ListView.Appearances.hpp>
#include <FMX.ListView.hpp>
#include <FMX.ListView.Types.hpp>
#include <FMX.MultiView.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.ListBox.hpp>
#include <FMX.Edit.hpp>
#include <FMX.Memo.hpp>
#include <FMX.ScrollBox.hpp>
#include <FMX.ImgList.hpp>
#include <Data.Bind.Components.hpp>
#include <Data.Bind.DBScope.hpp>
#include <Data.Bind.EngExt.hpp>
#include <Fmx.Bind.DBEngExt.hpp>
#include <Fmx.Bind.Editors.hpp>
#include <System.Bindings.Outputs.hpp>
#include <System.Rtti.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TTabControl *tc;
	TTabItem *tiList;
	TTabItem *tiItem;
	TToolBar *tbList;
	TSpeedButton *sbCheck;
	TSpeedButton *sbEdit;
	TLabel *laCaption;
	TButton *buAdd;
	TButton *buPopup;
	TListView *lv;
	TMultiView *mvPopup;
	TListBox *lbPopup;
	TListBoxItem *lbiAbout;
	TListBoxItem *lbiDeleteAll;
	TListBoxItem *lbiHelp;
	TGridPanelLayout *GridPanelLayout1;
	TLabel *laProductName;
	TEdit *edProductName;
	TLabel *laProductGroup;
	TLabel *laDescription;
	TMemo *meDescription;
	TLabel *laComplete;
	TSwitch *swComplete;
	TComboBox *cbProductGroup;
	TGridPanelLayout *GridPanelLayout2;
	TToolBar *ToolBar1;
	TButton *buItemBack;
	TLabel *laItemTitle;
	TButton *buDelete;
	TButton *buItemSave;
	TButton *buItemCancel;
	TLayout *laItemIcon;
	TLabel *laIcon;
	TButton *buIconLast;
	TButton *buIconNext;
	TButton *buIconDelete;
	TGlyph *Glyph;
	TBindSourceDB *BindSourceDB1;
	TBindingsList *BindingsList1;
	TLinkListControlToField *LinkListControlToField1;
	TLinkPropertyToField *LinkPropertyToFieldImageIndex;
	TLinkFillControlToField *LinkFillControlToField1;
	TLinkControlToField *LinkControlToField1;
	void __fastcall buAddClick(TObject *Sender);
	void __fastcall buItemBackClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
