//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "main.h"
#include "DataModule.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buAddClick(TObject *Sender)
{
    tc->ActiveTab = tiItem;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buItemBackClick(TObject *Sender)
{
    tc->ActiveTab = tiList;
}
//---------------------------------------------------------------------------

