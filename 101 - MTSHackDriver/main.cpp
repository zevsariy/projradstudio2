//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buUserFirstRunClick(TObject *Sender)
{
	tcMain->ActiveTab = tiUserReg;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buDriverFirstRunClick(TObject *Sender)
{
	tcMain->ActiveTab = tiDriverReg;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buAboutClick(TObject *Sender)
{
	ShowMessage(L"���������� ����������� � ������ �������� 16-17 ����� 2017 ����, �������� Unbreakable. ����������� ����, ����, ����, ������");
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buDriverStartClick(TObject *Sender)
{
	tcMain->ActiveTab = tiDriverReg;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::lvUserFindDriverItemClick(TObject * const Sender, TListViewItem * const AItem)

{
	tcMain->ActiveTab = tiUserFindDriverSelected;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::Button10Click(TObject *Sender)
{
	tcMain->ActiveTab = tiFirstRun;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::Button11Click(TObject *Sender)
{
	PrototypeBindSource1->Next();
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buUserLoginClick(TObject *Sender)
{
	tcMain->ActiveTab = tiDriver;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::laDriverRegLoginClick(TObject *Sender)
{
	 tcMain->ActiveTab = tiDriverLogin;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buDriverRegBackClick(TObject *Sender)
{
     tcMain->ActiveTab = tiFirstRun;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buUserRegBackClick(TObject *Sender)
{
	tcMain->ActiveTab = tiFirstRun;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buDriverLogoutClick(TObject *Sender)
{
	tcMain->ActiveTab = tiDriverLogin;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buUserLogoutClick(TObject *Sender)
{
	tcMain->ActiveTab = tiUserLogin;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buUserLoginBackClick(TObject *Sender)
{
   tcMain->ActiveTab = tiFirstRun;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buDriverLoginBackClick(TObject *Sender)
{
	tcMain->ActiveTab = tiFirstRun;
}
//---------------------------------------------------------------------------

