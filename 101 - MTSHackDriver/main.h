//---------------------------------------------------------------------------

#ifndef mainH
#define mainH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.Edit.hpp>
#include <FMX.ListBox.hpp>
#include <Data.Bind.Components.hpp>
#include <Data.Bind.EngExt.hpp>
#include <Data.Bind.GenData.hpp>
#include <Data.Bind.ObjectScope.hpp>
#include <Fmx.Bind.DBEngExt.hpp>
#include <Fmx.Bind.Editors.hpp>
#include <Fmx.Bind.GenData.hpp>
#include <FMX.ListView.Adapters.Base.hpp>
#include <FMX.ListView.Appearances.hpp>
#include <FMX.ListView.hpp>
#include <FMX.ListView.Types.hpp>
#include <System.Bindings.Outputs.hpp>
#include <System.Rtti.hpp>
#include <FMX.Objects.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TTabControl *tcMain;
	TTabItem *tiFirstRun;
	TTabItem *tiDriver;
	TTabItem *tiUser;
	TTabItem *tiUserReg;
	TTabItem *tiUserLogin;
	TTabItem *tiDriverReg;
	TTabItem *tiDriverLogin;
	TTabItem *tiUserFeedback;
	TTabItem *tiChatRing;
	TToolBar *tbFirstRun;
	TLabel *laFirstRunTitle;
	TButton *buAbout;
	TStyleBook *sb;
	TToolBar *ToolBar1;
	TLabel *laDriverTitle;
	TToolBar *ToolBar2;
	TLabel *laUserTitle;
	TToolBar *ToolBar3;
	TLabel *laUserRegTitle;
	TToolBar *ToolBar4;
	TLabel *laUserLoginTitle;
	TToolBar *ToolBar5;
	TLabel *laDriverRegTitle;
	TButton *buDriverRegBack;
	TToolBar *ToolBar6;
	TLabel *laDriverLoginTitle;
	TToolBar *ToolBar7;
	TLabel *laUserFeedbackTitle;
	TToolBar *ToolBar8;
	TLabel *laChatRingTitle;
	TGridPanelLayout *gplFirstRun;
	TButton *buUserFirstRun;
	TButton *buDriverFirstRun;
	TButton *buUserStart;
	TEdit *edDriverRegName;
	TLabel *laDriverRegPhone;
	TEdit *edDriverRegPhone;
	TLabel *laDriverRegName;
	TLabel *laDriverRegSex;
	TComboBox *cbDriverRegSex;
	TLabel *laDriverRegDriverTime;
	TEdit *edDriverRegDriverTime;
	TButton *buDriverRegLoadPhoto;
	TButton *buDriverRegSend;
	TTabItem *tiUserFindDriver;
	TToolBar *ToolBar9;
	TLabel *laUserFindDriverTitle;
	TButton *Button9;
	TListView *lvUserFindDriver;
	TPrototypeBindSource *PrototypeBindSource1;
	TBindingsList *BindingsList1;
	TLinkListControlToField *LinkListControlToField1;
	TTabItem *tiUserFindDriverSelected;
	TToolBar *ToolBar10;
	TLabel *laSelectedDriverProfileTitle;
	TButton *Button10;
	TLabel *laDriverName;
	TLabel *laDriverPhone;
	TLabel *laDriverNameInfo;
	TLabel *laDriverPhoneInfo;
	TImage *imDriverPhotoInfo;
	TLinkPropertyToField *LinkPropertyToFieldText;
	TLinkPropertyToField *LinkPropertyToFieldBitmap;
	TLinkPropertyToField *LinkPropertyToFieldText2;
	TButton *Button7;
	TButton *Button8;
	TButton *buDriverLoginBack;
	TButton *buUserLoginBack;
	TButton *buDriverLogout;
	TButton *buUserLogout;
	TButton *buUserRegBack;
	TButton *buDriverStart;
	TButton *Button11;
	TLabel *laUserLogin;
	TEdit *edUserLogin;
	TLabel *laUserPassword;
	TEdit *edUserPassword;
	TButton *buUserLogin;
	TLayout *Layout1;
	TLabel *Label1;
	TEdit *Edit1;
	TLabel *Label2;
	TEdit *Edit2;
	TButton *Button12;
	TLabel *laDriverRegLogin;
	void __fastcall buUserFirstRunClick(TObject *Sender);
	void __fastcall buDriverFirstRunClick(TObject *Sender);
	void __fastcall buAboutClick(TObject *Sender);
	void __fastcall buDriverStartClick(TObject *Sender);
	void __fastcall lvUserFindDriverItemClick(TObject * const Sender, TListViewItem * const AItem);
	void __fastcall Button10Click(TObject *Sender);
	void __fastcall Button11Click(TObject *Sender);
	void __fastcall buUserLoginClick(TObject *Sender);
	void __fastcall laDriverRegLoginClick(TObject *Sender);
	void __fastcall buDriverRegBackClick(TObject *Sender);
	void __fastcall buUserRegBackClick(TObject *Sender);
	void __fastcall buDriverLogoutClick(TObject *Sender);
	void __fastcall buUserLogoutClick(TObject *Sender);
	void __fastcall buUserLoginBackClick(TObject *Sender);
	void __fastcall buDriverLoginBackClick(TObject *Sender);

private:	// User declarations
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
