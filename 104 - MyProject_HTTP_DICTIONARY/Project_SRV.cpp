//----------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
#include <tchar.h>
#include <stdio.h>
#include <memory>
//----------------------------------------------------------------------------
USEFORM("dmuCommon.cpp", dmCommon); /* TDSServerModule: File Type */
USEFORM("dmu.cpp", dm); /* TDataModule: File Type */
USEFORM("dmuServer.cpp", dmServer); /* TDSServerModule: File Type */
USEFORM("dmuSession.cpp", dmSession); /* TDSServerModule: File Type */
USEFORM("dmuInvocation.cpp", dmInvocation); /* TDSServerModule: File Type */
//---------------------------------------------------------------------------
extern void runDSServer();
//----------------------------------------------------------------------------
#pragma argsused
int _tmain(int argc, _TCHAR* argv[])
{
  try
  {
    runDSServer();
  }
  catch (Exception &exception)
  {
    printf("%ls: %ls", exception.ClassName().c_str(), exception.Message.c_str());
  }
  return 0;
}
//----------------------------------------------------------------------------

