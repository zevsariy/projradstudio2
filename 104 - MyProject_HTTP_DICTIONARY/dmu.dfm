object dm: Tdm
  OldCreateOrder = False
  Height = 271
  Width = 415
  object DSServer1: TDSServer
    Left = 96
    Top = 11
  end
  object DSTCPServerTransport1: TDSTCPServerTransport
    Server = DSServer1
    Filters = <>
    AuthenticationManager = DSAuthenticationManager1
    Left = 96
    Top = 73
  end
  object DSHTTPService1: TDSHTTPService
    HttpPort = 8080
    Server = DSServer1
    Filters = <>
    AuthenticationManager = DSAuthenticationManager1
    Left = 96
    Top = 135
  end
  object DSAuthenticationManager1: TDSAuthenticationManager
    OnUserAuthenticate = DSAuthenticationManager1UserAuthenticate
    Roles = <
      item
        AuthorizedRoles.Strings = (
          'admins')
        ApplyTo.Strings = (
          'TdmSession.EchoString')
      end>
    Left = 96
    Top = 197
  end
  object DSServerClass_Server: TDSServerClass
    OnGetClass = DSServerClass_ServerGetClass
    Server = DSServer1
    LifeCycle = 'Server'
    Left = 200
    Top = 11
  end
  object DSServerClass_Session: TDSServerClass
    OnGetClass = DSServerClass_SessionGetClass
    Server = DSServer1
    Left = 240
    Top = 83
  end
  object DSServerClass_Invocation: TDSServerClass
    OnGetClass = DSServerClass_InvocationGetClass
    Server = DSServer1
    LifeCycle = 'Invocation'
    Left = 216
    Top = 155
  end
  object Client: TRESTClient
    Params = <>
    HandleRedirects = True
    Left = 336
    Top = 112
  end
  object Req: TRESTRequest
    Client = Client
    Params = <
      item
        name = 'app_id'
        Value = '7c1aad29'
      end
      item
        name = 'app_key'
        Value = '4a654b428a6af3866b820e771e9a193a'
      end>
    Response = Resp
    SynchronizedEvents = False
    Left = 344
    Top = 48
  end
  object Resp: TRESTResponse
    Left = 336
    Top = 176
  end
end
