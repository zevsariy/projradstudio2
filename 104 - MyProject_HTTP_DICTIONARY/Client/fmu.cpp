//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "fmu.h"
#include "uProxy.h"
#include "dmu.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buConnectClick(TObject *Sender)
{
	dm->SQLConnection1->Params->Values["DSAuthenticationUser"] = edLogin->Text;
    dm->SQLConnection1->Params->Values["DSAuthenticationPassword"] = edPassword->Text;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buServerGetIDClick(TObject *Sender)
{
    me->Lines->Add(dm->dmServerClient->GetID());
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buSessionGetIDClick(TObject *Sender)
{
    me->Lines->Add(dm->dmSessionClient->GetID());
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buInvocationGetIDClick(TObject *Sender)
{
	TdmInvocationClient *x;
	x = new TdmInvocationClient(dm->SQLConnection1->DBXConnection);
	try
	{
        me->Lines->Add(x->GetID());
	}
	__finally
	{
        x->Free();
    }
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buEchoClick(TObject *Sender)
{
    me->Lines->Add(dm->dmSessionClient->EchoString(edStr->Text));
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buReverseClick(TObject *Sender)
{
    me->Lines->Add(dm->dmSessionClient->ReverseString(edStr->Text));
}
//---------------------------------------------------------------------------
