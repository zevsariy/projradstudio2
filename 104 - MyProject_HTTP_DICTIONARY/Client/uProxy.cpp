// 
// Created by the DataSnap proxy generator.
// 02.05.2017 13:39:04
// 

#include "uProxy.h"

System::UnicodeString __fastcall TdmServerClient::GetID()
{
  if (FGetIDCommand == NULL)
  {
    FGetIDCommand = FDBXConnection->CreateCommand();
    FGetIDCommand->CommandType = TDBXCommandTypes_DSServerMethod;
    FGetIDCommand->Text = "TdmServer.GetID";
    FGetIDCommand->Prepare();
  }
  FGetIDCommand->ExecuteUpdate();
  System::UnicodeString result = FGetIDCommand->Parameters->Parameter[0]->Value->GetWideString();
  return result;
}

System::UnicodeString __fastcall TdmServerClient::EchoString(System::UnicodeString value)
{
  if (FEchoStringCommand == NULL)
  {
    FEchoStringCommand = FDBXConnection->CreateCommand();
    FEchoStringCommand->CommandType = TDBXCommandTypes_DSServerMethod;
    FEchoStringCommand->Text = "TdmServer.EchoString";
    FEchoStringCommand->Prepare();
  }
  FEchoStringCommand->Parameters->Parameter[0]->Value->SetWideString(value);
  FEchoStringCommand->ExecuteUpdate();
  System::UnicodeString result = FEchoStringCommand->Parameters->Parameter[1]->Value->GetWideString();
  return result;
}

System::UnicodeString __fastcall TdmServerClient::ReverseString(System::UnicodeString value)
{
  if (FReverseStringCommand == NULL)
  {
    FReverseStringCommand = FDBXConnection->CreateCommand();
    FReverseStringCommand->CommandType = TDBXCommandTypes_DSServerMethod;
    FReverseStringCommand->Text = "TdmServer.ReverseString";
    FReverseStringCommand->Prepare();
  }
  FReverseStringCommand->Parameters->Parameter[0]->Value->SetWideString(value);
  FReverseStringCommand->ExecuteUpdate();
  System::UnicodeString result = FReverseStringCommand->Parameters->Parameter[1]->Value->GetWideString();
  return result;
}


__fastcall  TdmServerClient::TdmServerClient(TDBXConnection *ADBXConnection)
{
  if (ADBXConnection == NULL)
    throw EInvalidOperation("Connection cannot be nil.  Make sure the connection has been opened.");
  FDBXConnection = ADBXConnection;
  FInstanceOwner = True;
}


__fastcall  TdmServerClient::TdmServerClient(TDBXConnection *ADBXConnection, bool AInstanceOwner)
{
  if (ADBXConnection == NULL) 
    throw EInvalidOperation("Connection cannot be nil.  Make sure the connection has been opened.");
  FDBXConnection = ADBXConnection;
  FInstanceOwner = AInstanceOwner;
}


__fastcall  TdmServerClient::~TdmServerClient()
{
  delete FGetIDCommand;
  delete FEchoStringCommand;
  delete FReverseStringCommand;
}

System::UnicodeString __fastcall TdmSessionClient::GetID()
{
  if (FGetIDCommand == NULL)
  {
    FGetIDCommand = FDBXConnection->CreateCommand();
    FGetIDCommand->CommandType = TDBXCommandTypes_DSServerMethod;
    FGetIDCommand->Text = "TdmSession.GetID";
    FGetIDCommand->Prepare();
}
FGetIDCommand->ExecuteUpdate();
System::UnicodeString result = FGetIDCommand->Parameters->Parameter[0]->Value->GetWideString();
return result;
}

System::UnicodeString __fastcall TdmSessionClient::EchoString(System::UnicodeString value)
{
  if (FEchoStringCommand == NULL)
  {
    FEchoStringCommand = FDBXConnection->CreateCommand();
    FEchoStringCommand->CommandType = TDBXCommandTypes_DSServerMethod;
    FEchoStringCommand->Text = "TdmSession.EchoString";
    FEchoStringCommand->Prepare();
}
FEchoStringCommand->Parameters->Parameter[0]->Value->SetWideString(value);
FEchoStringCommand->ExecuteUpdate();
System::UnicodeString result = FEchoStringCommand->Parameters->Parameter[1]->Value->GetWideString();
return result;
}

System::UnicodeString __fastcall TdmSessionClient::ReverseString(System::UnicodeString value)
{
  if (FReverseStringCommand == NULL)
  {
    FReverseStringCommand = FDBXConnection->CreateCommand();
    FReverseStringCommand->CommandType = TDBXCommandTypes_DSServerMethod;
    FReverseStringCommand->Text = "TdmSession.ReverseString";
    FReverseStringCommand->Prepare();
}
FReverseStringCommand->Parameters->Parameter[0]->Value->SetWideString(value);
FReverseStringCommand->ExecuteUpdate();
System::UnicodeString result = FReverseStringCommand->Parameters->Parameter[1]->Value->GetWideString();
return result;
}


__fastcall  TdmSessionClient::TdmSessionClient(TDBXConnection *ADBXConnection)
{
  if (ADBXConnection == NULL)
    throw EInvalidOperation("Connection cannot be nil.  Make sure the connection has been opened.");
FDBXConnection = ADBXConnection;
FInstanceOwner = True;
}


__fastcall  TdmSessionClient::TdmSessionClient(TDBXConnection *ADBXConnection, bool AInstanceOwner)
{
  if (ADBXConnection == NULL) 
    throw EInvalidOperation("Connection cannot be nil.  Make sure the connection has been opened.");
FDBXConnection = ADBXConnection;
FInstanceOwner = AInstanceOwner;
}


__fastcall  TdmSessionClient::~TdmSessionClient()
{
  delete FGetIDCommand;
  delete FEchoStringCommand;
  delete FReverseStringCommand;
}

System::UnicodeString __fastcall TdmInvocationClient::GetID()
{
  if (FGetIDCommand == NULL)
  {
    FGetIDCommand = FDBXConnection->CreateCommand();
    FGetIDCommand->CommandType = TDBXCommandTypes_DSServerMethod;
    FGetIDCommand->Text = "TdmInvocation.GetID";
    FGetIDCommand->Prepare();
}
FGetIDCommand->ExecuteUpdate();
System::UnicodeString result = FGetIDCommand->Parameters->Parameter[0]->Value->GetWideString();
return result;
}

System::UnicodeString __fastcall TdmInvocationClient::EchoString(System::UnicodeString value)
{
  if (FEchoStringCommand == NULL)
  {
    FEchoStringCommand = FDBXConnection->CreateCommand();
    FEchoStringCommand->CommandType = TDBXCommandTypes_DSServerMethod;
    FEchoStringCommand->Text = "TdmInvocation.EchoString";
    FEchoStringCommand->Prepare();
}
FEchoStringCommand->Parameters->Parameter[0]->Value->SetWideString(value);
FEchoStringCommand->ExecuteUpdate();
System::UnicodeString result = FEchoStringCommand->Parameters->Parameter[1]->Value->GetWideString();
return result;
}

System::UnicodeString __fastcall TdmInvocationClient::ReverseString(System::UnicodeString value)
{
  if (FReverseStringCommand == NULL)
  {
    FReverseStringCommand = FDBXConnection->CreateCommand();
    FReverseStringCommand->CommandType = TDBXCommandTypes_DSServerMethod;
    FReverseStringCommand->Text = "TdmInvocation.ReverseString";
    FReverseStringCommand->Prepare();
}
FReverseStringCommand->Parameters->Parameter[0]->Value->SetWideString(value);
FReverseStringCommand->ExecuteUpdate();
System::UnicodeString result = FReverseStringCommand->Parameters->Parameter[1]->Value->GetWideString();
return result;
}


__fastcall  TdmInvocationClient::TdmInvocationClient(TDBXConnection *ADBXConnection)
{
  if (ADBXConnection == NULL)
    throw EInvalidOperation("Connection cannot be nil.  Make sure the connection has been opened.");
FDBXConnection = ADBXConnection;
FInstanceOwner = True;
}


__fastcall  TdmInvocationClient::TdmInvocationClient(TDBXConnection *ADBXConnection, bool AInstanceOwner)
{
  if (ADBXConnection == NULL) 
    throw EInvalidOperation("Connection cannot be nil.  Make sure the connection has been opened.");
FDBXConnection = ADBXConnection;
FInstanceOwner = AInstanceOwner;
}


__fastcall  TdmInvocationClient::~TdmInvocationClient()
{
  delete FGetIDCommand;
  delete FEchoStringCommand;
  delete FReverseStringCommand;
}

