//----------------------------------------------------------------------------

#pragma hdrstop
#include <stdio.h>
#include <memory>

#include "dmu.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma classgroup "FMX.Controls.TControl"
#pragma resource "*.dfm"
Tdm *dm;
//---------------------------------------------------------------------------
__fastcall Tdm::Tdm(TComponent* Owner)
	: TDataModule(Owner)
{
	FInstanceOwner = true;
}

__fastcall Tdm::~Tdm()
{
	delete FdmServerClient;
	delete FdmSessionClient;
	delete FdmInvocationClient;
}

TdmServerClient* Tdm::GetdmServerClient(void)
{
	if (FdmServerClient == NULL)
	{
		SQLConnection1->Open();
		FdmServerClient = new TdmServerClient(SQLConnection1->DBXConnection, FInstanceOwner);
	}
	return FdmServerClient;
};
TdmSessionClient* Tdm::GetdmSessionClient(void)
{
	if (FdmSessionClient == NULL)
	{
		SQLConnection1->Open();
		FdmSessionClient = new TdmSessionClient(SQLConnection1->DBXConnection, FInstanceOwner);
	}
	return FdmSessionClient;
};
TdmInvocationClient* Tdm::GetdmInvocationClient(void)
{
	if (FdmInvocationClient == NULL)
	{
		SQLConnection1->Open();
		FdmInvocationClient = new TdmInvocationClient(SQLConnection1->DBXConnection, FInstanceOwner);
	}
	return FdmInvocationClient;
};

