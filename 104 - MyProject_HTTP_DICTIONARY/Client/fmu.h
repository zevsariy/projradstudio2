//---------------------------------------------------------------------------

#ifndef fmuH
#define fmuH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Edit.hpp>
#include <FMX.Memo.hpp>
#include <FMX.ScrollBox.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TButton *buConnect;
	TEdit *edLogin;
	TEdit *edPassword;
	TButton *buServerGetID;
	TButton *buSessionGetID;
	TButton *buInvocationGetID;
	TEdit *edStr;
	TButton *buEcho;
	TButton *buReverse;
	TMemo *me;
	void __fastcall buConnectClick(TObject *Sender);
	void __fastcall buServerGetIDClick(TObject *Sender);
	void __fastcall buSessionGetIDClick(TObject *Sender);
	void __fastcall buInvocationGetIDClick(TObject *Sender);
	void __fastcall buEchoClick(TObject *Sender);
	void __fastcall buReverseClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
