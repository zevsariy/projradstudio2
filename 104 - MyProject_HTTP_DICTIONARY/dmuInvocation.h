//---------------------------------------------------------------------------

#ifndef dmuInvocationH
#define dmuInvocationH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include "dmuCommon.h"
//---------------------------------------------------------------------------
class TdmInvocation : public TdmCommon
{
__published:	// IDE-managed Components
private:	// User declarations
public:		// User declarations
	__fastcall TdmInvocation(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TdmInvocation *dmInvocation;
//---------------------------------------------------------------------------
#endif
