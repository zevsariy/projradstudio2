//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "dmuCommon.h"
#include "dmu.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
//---------------------------------------------------------------------------
__fastcall TdmCommon::TdmCommon(TComponent* Owner)
	: TDSServerModule(Owner)
{
	TGUID x;
	OleCheck(CoCreateGuid(&x));
	GUID = Sysutils::GUIDToString(x);
}

System::UnicodeString TdmCommon::GetID()
{
    return GUID;
}
//----------------------------------------------------------------------------
System::UnicodeString TdmCommon::EchoString(System::UnicodeString value)
{
    return value;
}
//----------------------------------------------------------------------------
System::UnicodeString TdmCommon::ReverseString(System::UnicodeString value)
{
    return ::ReverseString(value);
}
//----------------------------------------------------------------------------
UnicodeString TdmCommon::GetDefinition(UnicodeString value)
{
	value = dm->GetDefinition("Russia");
	return value;
}
//----------------------------------------------------------------------------

