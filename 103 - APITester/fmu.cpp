//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "fmu.h"
#include "dmu.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buSendClick(TObject *Sender)
{
	dm->rReq->ClearBody();
	dm->rReq->Params->Clear();

	UnicodeString meString;
	for(int i=0; i < meReq->Lines->Count; i++)
	{
		meString += meReq->Lines->Strings[i];
	}
	dm->rReq->AddParameter("jsonka", meString);
	dm->rReq->AddParameter("test", "test");
	dm->rReq->Execute();
	meResp->Lines->Clear();
	meResp->Lines->Add(dm->rResp->Content);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::swNewOrOldSwitch(TObject *Sender)
{
	if(swNewOrOld->IsChecked)
	{
		laApiAge->Text = L"����� API";
		dm->rClient->BaseURL = "http://bmw.2tsy.ru/api/v2/index.php";
	}
	else
	{
		laApiAge->Text = L"������ API";
		dm->rClient->BaseURL = "http://bmw.2tsy.ru/api/index.php";
	}
}
//---------------------------------------------------------------------------

