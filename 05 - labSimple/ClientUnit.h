//---------------------------------------------------------------------------

#ifndef ClientUnitH
#define ClientUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.ActnList.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Edit.hpp>
#include <FMX.Memo.hpp>
#include <FMX.ScrollBox.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <IdBaseComponent.hpp>
#include <IdComponent.hpp>
#include <IdTCPClient.hpp>
#include <IdTCPConnection.hpp>
#include <System.Actions.hpp>
#include <FMX.Objects.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TToolBar *ToolBar1;
	TIdTCPClient *tcpClient;
	TButton *buConnect;
	TButton *buDIsconnect;
	TButton *buGetTime;
	TButton *buGetFio;
	TEdit *edHost;
	TEdit *edPort;
	TMemo *meClient;
	TActionList *ac;
	TAction *acConnect;
	TAction *acDisconnect;
	TAction *acGetTime;
	TAction *acGetFio;
	TButton *buGetInfo;
	TAction *acGetInfo;
	TAction *acGetImage;
	TImage *im;
	TButton *buGetImage;
	void __fastcall acGetFioExecute(TObject *Sender);
	void __fastcall acConnectExecute(TObject *Sender);
	void __fastcall acDisconnectExecute(TObject *Sender);
	void __fastcall acGetTimeExecute(TObject *Sender);
	void __fastcall acUpdate(TBasicAction *Action, bool &Handled);
	void __fastcall acGetInfoExecute(TObject *Sender);
	void __fastcall acGetImageExecute(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
