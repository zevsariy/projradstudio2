//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "ClientUnit.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tfm::acGetFioExecute(TObject *Sender)
{
	tcpClient->Socket->WriteLn("fio");
	UnicodeString x;
	x = tcpClient->Socket->ReadLn(IndyTextEncoding_UTF8());
	meClient->Lines->Add(x);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::acConnectExecute(TObject *Sender)
{
	tcpClient->Host = edHost->Text;
	tcpClient->Port = StrToInt(edPort->Text);
	tcpClient->Connect();
}
//---------------------------------------------------------------------------
void __fastcall Tfm::acDisconnectExecute(TObject *Sender)
{
    tcpClient->Disconnect();
}
//---------------------------------------------------------------------------
void __fastcall Tfm::acGetTimeExecute(TObject *Sender)
{
	tcpClient->Socket->WriteLn("time");
	UnicodeString x;
	x = tcpClient->Socket->ReadLn();
	meClient->Lines->Add(x);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::acUpdate(TBasicAction *Action, bool &Handled)
{
	edHost->Enabled = ! tcpClient->Connected();
	edPort->Enabled = ! tcpClient->Connected();
	acConnect->Enabled = ! tcpClient->Connected();
	acDisconnect->Enabled = tcpClient->Connected();
	acGetTime->Enabled = tcpClient->Connected();
	acGetFio->Enabled = tcpClient->Connected();
}
//---------------------------------------------------------------------------
void __fastcall Tfm::acGetInfoExecute(TObject *Sender)
{
    tcpClient->Socket->WriteLn("info");
	UnicodeString x;
	x = tcpClient->Socket->ReadLn();
	meClient->Lines->Add(x);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::acGetImageExecute(TObject *Sender)
{
	tcpClient->Socket->WriteLn("image");
	TMemoryStream *x = new TMemoryStream();
	try
	{
		int xSize = tcpClient->Socket->ReadInt64();
		tcpClient->Socket->ReadStream(x,xSize);
		im->Bitmap->LoadFromStream(x);
	}
	__finally
	{
        delete x;
    }
}
//---------------------------------------------------------------------------

