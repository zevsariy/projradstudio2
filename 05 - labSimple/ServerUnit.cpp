//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "ServerUnit.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buStartClick(TObject *Sender)
{
	tcpServer->Active = true;
	meServer->Lines->Add(L"����������� �����������.");
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buStopClick(TObject *Sender)
{
	tcpServer->Active = false;
	meServer->Lines->Add(L"����������� �������������.");
}
//---------------------------------------------------------------------------
void __fastcall Tfm::tcpServerConnect(TIdContext *AContext)
{
    meServer->Lines->Add(L"������ ��������� � ������������");
}

//---------------------------------------------------------------------------
void __fastcall Tfm::tcpServerExecute(TIdContext *AContext)
{
	 UnicodeString x = AContext->Connection->Socket->ReadLn();
	 meServer->Lines->Add(L"������: " + x);
	 if(x == "time")
	 {
		 AContext->Connection->Socket->WriteLn(TimeToStr(Now()));
	 }
	 else if(x == "fio")
	 {
		 AContext->Connection->Socket->WriteLn(L"������ ������� ��������", IndyTextEncoding_UTF8());
	 }
	 else if(x == "info")
	 {
		 AContext->Connection->Socket->WriteLn(L"������: " +
		 tcpServer->Version, IndyTextEncoding_UTF8());
	 }
	 else if(x == "image")
	 {
		TMemoryStream *x = new TMemoryStream();
		try
		{
			im->Bitmap->SaveToStream(x);
			x->Seek(0,0);
			AContext->Connection->Socket->Write(x->Size);
			AContext->Connection->Socket->Write(x);
		}
		__finally
		{
			delete x;
		}
	 }
}
//---------------------------------------------------------------------------
