//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "fmu.h"
#include "dmu.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}

void Tfm::NewImage()
{
	NewImage(Random(dm->il->Count), Random(ly->Width - 50), Random(ly->Height - 50), 50, 50);
}

void Tfm::ClearPostcard()
{
	ReSetSelection(this);
	for(int i = ly->ComponentCount-1; i>= 0; i--)
	{
		ly->Controls->Items[i]->Name;
		if(dynamic_cast<TSelection*>(ly->Controls->Items[i]))
		{
			ly->RemoveObject(i);
		}
	}
	glyphBG->ImageIndex = -1;
	FNamePostcard = "";
}

void Tfm::SavePostcard(UnicodeString aName)
{
	ReSetSelection(this);
	lyPostcard->Visible = false;
	lyPostcard->Align = TAlignLayout::None;
	lyPostcard->Width = cSavingWH;
	lyPostcard->Height = cSavingWH;
	UnicodeString xFileName = System::Ioutils::TPath::Combine(System::Ioutils::TPath::GetDocumentsPath(), aName + cExtSAVE);
	UnicodeString xFileNamePNG = System::Ioutils::TPath::ChangeExtension(xFileName, cExtPNG);

	TBinaryWriter *f;
	ly->MakeScreenshot()->SaveToFile(xFileNamePNG);
	f = new TBinaryWriter(xFileName, false, TEncoding::UTF8);
	try
	{
		f->Write(glyphBG->ImageIndex);
		for(int i=0; i < ly->ControlsCount; i++)
		{
			TSelection *x = dynamic_cast<TSelection*>(ly->Controls->Items[i]);
			if(x != NULL)
			{
				f->Write(((TGlyph*)x->Components[0])->ImageIndex);
				f->Write(x->Position->X);
				f->Write(x->Position->Y);
				f->Write(x->Width);
				f->Write(x->Height);
			}
		}
	}
	__finally {
		f->DisposeOf();
	}

	FNamePostcard = aName;
	lyPostcard->Align = TAlignLayout::Client;
	lyPostcard->Visible = true;
}

void Tfm::OpenPostcard(UnicodeString aName)
{
	ClearPostcard();
	lyPostcard->Visible = false;
	lyPostcard->Align = TAlignLayout::None;
	lyPostcard->Width = cSavingWH;
	lyPostcard->Height = cSavingWH;
	UnicodeString xFileName = System::Ioutils::TPath::Combine(System::Ioutils::TPath::GetDocumentsPath(), aName + cExtSAVE);

	TBinaryReader *f;
	f = new TBinaryReader(xFileName, TEncoding::UTF8);
	try
	{
		glyphBG->ImageIndex = f->ReadInteger();
		while (f->PeekChar() > -1)
		{
			int xIndex = f->ReadInteger();
			float xX = f->ReadSingle();
			float xY = f->ReadSingle();
			float xWidth = f->ReadSingle();
			float xHeight = f->ReadSingle();
			NewImage(xIndex, xX, xY, xWidth, xHeight);
		}
	}
	__finally {
		f->DisposeOf();
	}
	ReSetSelection(this);
	FNamePostcard = aName;
	lyPostcard->Align = TAlignLayout::Client;
	lyPostcard->Visible = true;
}

void Tfm::ReloadOpenList()
{
	DynamicArray<UnicodeString> x;
	TListViewItem *xItem;
	x = System::Ioutils::TDirectory::GetFiles(System::Ioutils::TPath::GetDocumentsPath(), "*" + cExtSAVE);
	lvOpen->BeginUpdate();
	try {
		lvOpen->Items->Clear();
		for (int i = 0; i < x.Length; i++)
		{
			xItem = lvOpen->Items->Add();
			xItem->Text = System::Ioutils::TPath::GetFileNameWithoutExtension(x[i]);
		}
	}
	__finally
	{
        lvOpen->EndUpdate();
    }
}
//---------------------------------------------------------------------------
void Tfm::ReSetSelection(TObject *Sender)
{
	FSel = dynamic_cast<TSelection*>(Sender);
	TSelection *x;
	for (int i = 0; i < ly->ComponentCount; i++) {
		x = dynamic_cast<TSelection*>(ly->Components[i]);
		if (x) {
			x->HideSelection = (x != FSel);
		}
	}
	//
	tbPostcardIM->Visible = (FSel != NULL);
	tbPostcardBG->Visible = ! tbPostcardIM->Visible;
}
//---------------------------------------------------------------------------
void Tfm::NewImage(int aIndex, float aX, float aY, float aWidth, float aHeight)
{
	TSelection *x = new TSelection(ly);
	x->Parent = ly;
	x->Width = aWidth;
	x->Height = aHeight;
	x->Position->X = aX;
	x->Position->Y = aY;
	x->OnMouseDown = SelectionAllMouseDown;
	x->GripSize = 10;
	x->Align = TAlignLayout::Scale;
	TGlyph *xGlyph = new TGlyph(x);
	xGlyph->Parent = x;
	xGlyph->Align = TAlignLayout::Client;
	xGlyph->Images = dm->il;
	xGlyph->ImageIndex = aIndex;
	//
	ReSetSelection(x);
}
//---------------------------------------------------------------------------
void Tfm::ReloadIM()
{
	//
	for(int i = 0; i < dm->il->Count-1; i++){
		TRectangle *x = new TRectangle(glIM);
		x->Parent = glIM;
		x->Fill->Kind = TBrushKind::None;
		x->Stroke->Kind = TBrushKind::None;
		x->OnMouseDown = RectangleAllMouseDown;
		x->OnDblClick = RectangleAllDblClick;
		TGlyph *xGlyph = new TGlyph(x);
		xGlyph->Parent = x;
		xGlyph->Align = TAlignLayout::Client;
		xGlyph->Images = dm->il;
		xGlyph->ImageIndex = i;
	}
	glIM->RecalcSize();
}
//---------------------------------------------------------------------------
void Tfm::ShowTabIM(TImageIndex aIndex)
{
	for (int i = 0; i < glIM->ComponentCount-1; i++) {
		if (((TGlyph*)glIM->Components[i]->Components[0])->ImageIndex == aIndex) {
			ReSetIM(glIM->Components[i]);
			break;
		}
	}
	tc->ActiveTab = tiIM;
}
//---------------------------------------------------------------------------
void Tfm::ReSetIM(TObject *Sender)
{
	if (FSelIM != NULL) {
		FSelIM->Stroke->Kind = TBrushKind::None;
	}
	FSelIM = dynamic_cast<TRectangle*>(Sender);
	FSelIM->Stroke->Kind = TBrushKind::Solid;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::SelectionAllMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift, float X, float Y)
{
	ReSetSelection(Sender);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::RectangleAllMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift, float X, float Y)
{
	ReSetIM(Sender);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::RectangleAllDblClick(TObject *Sender)
{
	buChoiceIMClick(Sender);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::FormCreate(TObject *Sender)
{
	tc->ActiveTab = tiMenu;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::FormShow(TObject *Sender)
{
	ReSetSelection(Sender);
	ReloadIM();
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buCreateClick(TObject *Sender)
{
	tc->ActiveTab = tiPostcard;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buBackClick(TObject *Sender)
{
	tc->ActiveTab = tiMenu;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buAddClick(TObject *Sender)
{
	NewImage();
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buPrevBGClick(TObject *Sender)
{
	glyphBG->ImageIndex = ((int)glyphBG->ImageIndex <= 0) ?
		dm->ilBG->Count - 1 : (int)glyphBG->ImageIndex - 1;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buNextBGClick(TObject *Sender)
{
	glyphBG->ImageIndex = ((int)glyphBG->ImageIndex >= dm->ilBG->Count - 1) ?
		0 : (int)glyphBG->ImageIndex + 1;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buRandomBGClick(TObject *Sender)
{
	glyphBG->ImageIndex = Random(dm->ilBG->Count);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buPrevIMClick(TObject *Sender)
{
	TGlyph *x = (TGlyph*)FSel->Components[0];
	x->ImageIndex = ((int)x->ImageIndex <= 0) ? dm->il->Count - 1 : (int)x->ImageIndex - 1;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buNextIMClick(TObject *Sender)
{
	TGlyph *x = (TGlyph*)FSel->Components[0];
	x->ImageIndex = ((int)x->ImageIndex >= dm->il->Count - 1) ? 0 : (int)x->ImageIndex + 1;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buSelectIMClick(TObject *Sender)
{
	ShowTabIM(((TGlyph*)FSel->Components[0])->ImageIndex);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buRandomIMClick(TObject *Sender)
{
	((TGlyph*)FSel->Components[0])->ImageIndex = Random(dm->il->Count);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buDelClick(TObject *Sender)
{
    ReloadOpenList();
	FSel->DisposeOf();
	FSel = NULL;
	ReSetSelection(FSel);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::lyPostcardMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift, float X, float Y)
{
	ReSetSelection(Sender);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buBackIMClick(TObject *Sender)
{
	tc->ActiveTab = tiPostcard;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buIMOutClick(TObject *Sender)
{
	glIM->ItemHeight -=5;
	glIM->ItemWidth -=5;
	sbIM->Height -= 5;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buIMInClick(TObject *Sender)
{
	glIM->ItemHeight +=5;
	glIM->ItemWidth +=5;
	sbIM->Height +=5;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buChoiceIMClick(TObject *Sender)
{
	if (FSel != NULL && FSelIM != NULL) {
		((TGlyph*)FSel->Components[0])->ImageIndex = ((TGlyph*)FSelIM->Components[0])->ImageIndex;
	}
	tc->ActiveTab = tiPostcard;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::glIMResize(TObject *Sender)
{
	int x = 0;
	for (int i = 0; i < glIM->ComponentCount-1; i++) {
			x += dynamic_cast<TRectangle*>(glIM->Components[i])->Height;
	}
	glIM->Height = x / (glIM->Width / glIM->ItemWidth);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::acShowShareSheetActionBeforeExecute(TObject *Sender)
{
	acShowShareSheetAction->Bitmap->Assign(ly->MakeScreenshot());
}
//---------------------------------------------------------------------------


void __fastcall Tfm::buEditSaveClick(TObject *Sender)
{
    tc->ActiveTab = tiSave;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buEditOpenClick(TObject *Sender)
{
    tc->ActiveTab = tiOpen;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buSaveBackClick(TObject *Sender)
{
    tc->ActiveTab = tiPostcard;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buOpenBackClick(TObject *Sender)
{
    tc->ActiveTab = tiMenu;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buOpenDeleteClick(TObject *Sender)
{
	ClearPostcard();	
}
//---------------------------------------------------------------------------

void __fastcall Tfm::tcChange(TObject *Sender)
{
	if(tc->ActiveTab == tiOpen)
	{	
		 ReloadOpenList();
	}
	if(tc->ActiveTab == tiSave) 
	{
		 edSaveName->Text = FNamePostcard;
	}
}
//---------------------------------------------------------------------------

void __fastcall Tfm::lvOpenChange(TObject *Sender)
{
	if(lvOpen->ItemIndex != -1)
	{	
		imOpen->Bitmap->LoadFromFile(System::Ioutils::TPath::Combine(System::Ioutils::TPath::GetDocumentsPath(), lvOpen->Items->AppearanceItem[lvOpen->ItemIndex]->Text + cExtPNG));
	}
	else
	{
        imOpen->Bitmap->SetSize(0,0);
    }
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buOpenOpenClick(TObject *Sender)
{
	if(lvOpen->ItemIndex != -1)
	{	
		OpenPostcard(lvOpen->Items->AppearanceItem[lvOpen->ItemIndex]->Text);
		tc->ActiveTab = tiPostcard;
    }
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buSaveSaveClick(TObject *Sender)
{
	if(edSaveName->Text.IsEmpty())
	{	
		ShowMessage(L"������� ���");
	}
	else
	{
		SavePostcard(edSaveName->Text);
		ShowMessage(L"���������");
		tc->ActiveTab =  tiPostcard;
    }
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buLoadClick(TObject *Sender)
{
	tc->ActiveTab = tiOpen;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buAboutClick(TObject *Sender)
{
	ShowMessage(L"����� �� ������ ��������, �� �� �� ����� ���������� � ��������� �������");
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buHelpClick(TObject *Sender)
{
   ShowMessage(L"��� ���� ������������� ��������, �� �������� ���������� ����� ��� � �������. �� � ������ ���� ��� ������ ����.");
}
//---------------------------------------------------------------------------

