object dm: Tdm
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 271
  Width = 415
  object SQLConnection1: TSQLConnection
    DriverName = 'DataSnap'
    LoginPrompt = False
    Params.Strings = (
      'DriverUnit=Data.DBXDataSnap'
      'HostName=localhost'
      
        'DriverAssemblyLoader=Borland.Data.TDBXClientDriverLoader,Borland' +
        '.Data.DbxClientDriver,Version=24.0.0.0,Culture=neutral,PublicKey' +
        'Token=91d62ebb5b0d1b1b'
      'Port=211'
      'CommunicationProtocol=tcp/ip'
      'DatasnapContext=datasnap/'
      'Filters={}')
    Connected = True
    Left = 48
    Top = 40
  end
  object DSProviderConnection1: TDSProviderConnection
    ServerClassName = 'TdmAAA'
    Connected = True
    SQLConnection = SQLConnection1
    Left = 48
    Top = 96
  end
  object cdsCategory: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dspCategory'
    RemoteServer = DSProviderConnection1
    AfterPost = cdsCategoryAfterPost
    AfterScroll = cdsCategoryAfterScroll
    Left = 176
    Top = 48
    object cdsCategoryID: TIntegerField
      FieldName = 'ID'
      Required = True
    end
    object cdsCategoryNAME: TWideStringField
      FieldName = 'NAME'
      Required = True
      Size = 200
    end
  end
  object cdsService: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dspService'
    RemoteServer = DSProviderConnection1
    AfterPost = cdsServiceAfterPost
    Left = 176
    Top = 112
    object cdsServiceID: TIntegerField
      FieldName = 'ID'
      Required = True
    end
    object cdsServiceCATEGORY_ID: TIntegerField
      FieldName = 'CATEGORY_ID'
      Required = True
    end
    object cdsServiceNAME: TWideStringField
      FieldName = 'NAME'
      Required = True
      Size = 240
    end
    object cdsServicePRICE: TSingleField
      FieldName = 'PRICE'
    end
    object cdsServiceDURATION: TIntegerField
      FieldName = 'DURATION'
    end
  end
end
