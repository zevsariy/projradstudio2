//---------------------------------------------------------------------------

#ifndef dmuAAAH
#define dmuAAAH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <DataSnap.DSServer.hpp>
#include <Datasnap.DSProviderDataModuleAdapter.hpp>
#include <Data.DB.hpp>
#include <Data.DBXFirebird.hpp>
#include <Data.FMTBcd.hpp>
#include <Data.SqlExpr.hpp>
#include <Datasnap.Provider.hpp>
//---------------------------------------------------------------------------
class TdmAAA : public TDSServerModule
{
__published:	// IDE-managed Components
	TDataSetProvider *dspService;
	TDataSetProvider *dspCategory;
	TSQLConnection *BeautysaloonConnection;
	TSQLDataSet *CategoryTable;
	TSQLDataSet *ServiceTable;
private:	// User declarations
public:		// User declarations
	__fastcall TdmAAA(TComponent* Owner);
	System::UnicodeString EchoString(System::UnicodeString value);
	System::UnicodeString  ReverseString(System::UnicodeString value);
};
//---------------------------------------------------------------------------
extern PACKAGE TdmAAA *dmAAA;
//---------------------------------------------------------------------------
#endif

