//----------------------------------------------------------------------------
#include <fmx.h>
#pragma hdrstop
#include <tchar.h>
#include <stdio.h>
#include <memory>
#include "dmuAAA.h"

#include "dmu.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma classgroup "FMX.Controls.TControl"
#pragma resource "*.dfm"
Tdm *dm;
//---------------------------------------------------------------------------
__fastcall Tdm::Tdm(TComponent* Owner)
	: TDataModule(Owner)
{
}
//----------------------------------------------------------------------------
void __fastcall Tdm::DSServerClass1GetClass(TDSServerClass *DSServerClass,
          TPersistentClass &PersistentClass)
{
	PersistentClass =  __classid(TdmAAA);
}
//----------------------------------------------------------------------------
void __fastcall Tdm::DSAuthenticationManager1UserAuthenticate(TObject *Sender,
          const UnicodeString Protocol, const UnicodeString Context, const UnicodeString User,
          const UnicodeString Password, bool &valid, TStrings *UserRoles)

{
	/* TODO : Validate the client user and password.
	If role-based authorization is needed, add role names to the UserRoles parameter  */
	valid = true;
}
//----------------------------------------------------------------------------
//---------------------------------------------------------------------------

