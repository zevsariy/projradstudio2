//---------------------------------------------------------------------------

#ifndef dmuServerH
#define dmuServerH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <DataSnap.DSServer.hpp>
#include <Datasnap.DSProviderDataModuleAdapter.hpp>
#include <Data.DB.hpp>
#include <Data.DBXFirebird.hpp>
#include <Data.FMTBcd.hpp>
#include <Data.SqlExpr.hpp>
#include <Datasnap.Provider.hpp>
//---------------------------------------------------------------------------
class TdmServer : public TDSServerModule
{
__published:	// IDE-managed Components
	TSQLConnection *MyprojectapiConnection;
	TSQLDataSet *UsersTable;
	TDataSetProvider *dspUsers;
private:	// User declarations
public:		// User declarations
	__fastcall TdmServer(TComponent* Owner);
	System::UnicodeString EchoString(System::UnicodeString value);
	System::UnicodeString  ReverseString(System::UnicodeString value);
};
//---------------------------------------------------------------------------
extern PACKAGE TdmServer *dmServer;
//---------------------------------------------------------------------------
#endif

