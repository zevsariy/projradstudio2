//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "fruContact"
#pragma link "fruEnd"
#pragma link "fruPerson"
#pragma link "fruStart"
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}

void Tfm::HideToolbar()
{
	if(tc->TabIndex == 0)
	{
		  tb->Visible = false;
	}
	else
	{
          tb->Visible = true;
    }
}
//---------------------------------------------------------------------------
void __fastcall Tfm::FormShow(TObject *Sender)
{
	tc->First();
	HideToolbar();
}
//---------------------------------------------------------------------------

void __fastcall Tfm::tcChange(TObject *Sender)
{
	HideToolbar();
}
//---------------------------------------------------------------------------

