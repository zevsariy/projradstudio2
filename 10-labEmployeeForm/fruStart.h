//---------------------------------------------------------------------------

#ifndef fruStartH
#define fruStartH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
//---------------------------------------------------------------------------
class TfrStart : public TFrame
{
__published:	// IDE-managed Components
	TLabel *Label1;
	TButton *buBegin;
	TButton *buClose;
	void __fastcall buBeginClick(TObject *Sender);
	void __fastcall buCloseClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TfrStart(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TfrStart *frStart;
//---------------------------------------------------------------------------
#endif
