//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "fruEnd.h"
#include "Main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TfrEnd *frEnd;
//---------------------------------------------------------------------------
__fastcall TfrEnd::TfrEnd(TComponent* Owner)
	: TFrame(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TfrEnd::buAgainClick(TObject *Sender)
{
	fm->tc->TabIndex = 1;
	fm->HideToolbar();
}
//---------------------------------------------------------------------------
