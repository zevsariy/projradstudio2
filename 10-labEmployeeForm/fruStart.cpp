//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "fruStart.h"
#include "Main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TfrStart *frStart;
//---------------------------------------------------------------------------
__fastcall TfrStart::TfrStart(TComponent* Owner)
	: TFrame(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TfrStart::buBeginClick(TObject *Sender)
{
	fm->tc->TabIndex = 1;
	fm->HideToolbar();
}
//---------------------------------------------------------------------------
void __fastcall TfrStart::buCloseClick(TObject *Sender)
{
    fm->Close();
}
//---------------------------------------------------------------------------
