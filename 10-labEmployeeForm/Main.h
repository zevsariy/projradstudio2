//---------------------------------------------------------------------------

#ifndef MainH
#define MainH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include "fruContact.h"
#include "fruEnd.h"
#include "fruPerson.h"
#include "fruStart.h"
#include <FMX.ActnList.hpp>
#include <System.Actions.hpp>
#include <FMX.Gestures.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TToolBar *tb;
	TButton *buPrev;
	TButton *buNext;
	TTabControl *tc;
	TTabItem *tiStart;
	TTabItem *tiPerson;
	TTabItem *tiContact;
	TTabItem *tiEnd;
	TfrStart *Start1;
	TfrEnd *End1;
	TfrContact *Contact1;
	TfrPerson *Person1;
	TActionList *ac;
	TNextTabAction *NextTabAction1;
	TPreviousTabAction *PreviousTabAction1;
	TGestureManager *gm;
	void __fastcall FormShow(TObject *Sender);
	void __fastcall tcChange(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
	void HideToolbar();
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
