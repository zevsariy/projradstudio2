//---------------------------------------------------------------------------

#ifndef fruContactH
#define fruContactH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Edit.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.ListBox.hpp>
#include <FMX.Types.hpp>
//---------------------------------------------------------------------------
class TfrContact : public TFrame
{
__published:	// IDE-managed Components
	TListBox *ListBox1;
	TListBoxItem *ListBoxItem1;
	TEdit *Edit1;
	TListBoxItem *ListBoxItem2;
	TEdit *Edit2;
	TListBoxItem *ListBoxItem3;
	TEdit *Edit3;
private:	// User declarations
public:		// User declarations
	__fastcall TfrContact(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TfrContact *frContact;
//---------------------------------------------------------------------------
#endif
