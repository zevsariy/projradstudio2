//---------------------------------------------------------------------------

#ifndef fruEndH
#define fruEndH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
//---------------------------------------------------------------------------
class TfrEnd : public TFrame
{
__published:	// IDE-managed Components
	TLabel *Label1;
	TButton *buAgain;
	TButton *buExit;
	void __fastcall buAgainClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TfrEnd(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TfrEnd *frEnd;
//---------------------------------------------------------------------------
#endif
