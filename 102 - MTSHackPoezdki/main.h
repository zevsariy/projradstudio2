//---------------------------------------------------------------------------

#ifndef mainH
#define mainH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <FMX.Edit.hpp>
#include <FMX.ListBox.hpp>
#include <FMX.Objects.hpp>
#include <FMX.ListView.Adapters.Base.hpp>
#include <FMX.ListView.Appearances.hpp>
#include <FMX.ListView.hpp>
#include <FMX.ListView.Types.hpp>
#include <FMX.Memo.hpp>
#include <FMX.ScrollBox.hpp>
#include <Data.Bind.Components.hpp>
#include <Data.Bind.EngExt.hpp>
#include <Data.Bind.GenData.hpp>
#include <Data.Bind.ObjectScope.hpp>
#include <Fmx.Bind.DBEngExt.hpp>
#include <Fmx.Bind.Editors.hpp>
#include <Fmx.Bind.GenData.hpp>
#include <System.Bindings.Outputs.hpp>
#include <System.Rtti.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TTabControl *tc;
	TTabItem *tiFirstRun;
	TToolBar *tbFR;
	TButton *buFRAbout;
	TLabel *laFRTitle;
	TGridPanelLayout *gplFR;
	TButton *buFRUser;
	TButton *buFRDriver;
	TButton *buFRLogin;
	TTabItem *tiUserReg;
	TToolBar *tbUR;
	TLabel *laURTitle;
	TButton *buURBack;
	TScrollBox *sbUR;
	TLabel *laURName;
	TEdit *edURName;
	TLabel *laURPhoto;
	TImage *imURPhoto;
	TLabel *laURPhone;
	TEdit *edURPhone;
	TLabel *laURPassword;
	TEdit *edURPassword;
	TLabel *laURPasswordR;
	TEdit *edURPasswordR;
	TLabel *laURInterest;
	TCheckBox *cbURInterest2;
	TCheckBox *cbURInterest4;
	TCheckBox *cbURInterest3;
	TCheckBox *cbURInterest1;
	TButton *buURReg;
	TTabItem *tiDriverReg;
	TScrollBox *sbDR;
	TLabel *laDRName;
	TEdit *edDRName;
	TLabel *laDRPhoto;
	TImage *imDRPhoto;
	TLabel *laDRPhone;
	TEdit *edDRPhone;
	TLabel *������;
	TEdit *edDRPassword;
	TLabel *laDRPasswordR;
	TEdit *edDRPasswordR;
	TLabel *laDRInterest;
	TCheckBox *cbDRInterest2;
	TCheckBox *cbDRInterest4;
	TCheckBox *cbDRInterest3;
	TCheckBox *cbDRInterest1;
	TButton *buDRReg;
	TToolBar *tbDR;
	TLabel *laDRTitle;
	TButton *buDRBack;
	TLabel *laDRStage;
	TComboBox *cbDRStage;
	TTabItem *tiUser;
	TToolBar *tbU;
	TLabel *laUTitle;
	TButton *buUEdit;
	TButton *buULogout;
	TButton *buUStart;
	TLabel *laURoadCur;
	TLabel *laURoad;
	TButton *buUOrders;
	TTabItem *tiDriver;
	TToolBar *tbD;
	TLabel *laDTitle;
	TButton *buDEdit;
	TButton *buDLogout;
	TButton *buDOrders;
	TLabel *laDRoad;
	TLabel *laDRoadCur;
	TListView *lvD;
	TTabItem *tiLogin;
	TToolBar *tbL;
	TLabel *laLTitle;
	TButton *buLBack;
	TSwitch *swDU;
	TLabel *laLLogin;
	TLabel *laLPassword;
	TLabel *laDU;
	TEdit *edLPassword;
	TEdit *edLLogin;
	TCheckBox *cbLRemember;
	TButton *buLLogin;
	TLabel *laLReg;
	TLayout *laL;
	TTabItem *tiUserDriversList;
	TToolBar *tbUDL;
	TLabel *laUDLTitle;
	TButton *buUDLBack;
	TListView *lvUDL;
	TTabItem *tiDriverInfo;
	TToolBar *tbDI;
	TLabel *laDITitle;
	TButton *buDIBack;
	TLabel *laDIName;
	TButton *buDIFeedbacks;
	TButton *buDIChat;
	TImage *imDIPhoto;
	TLabel *laDIRoad;
	TLabel *laDIRoadCur;
	TButton *buDIAgree;
	TButton *buDIEdit;
	TButton *buDINext;
	TLabel *laDUCost;
	TTabItem *tiUserFeedBacks;
	TToolBar *tbUFB;
	TLabel *laUFBTitle;
	TButton *buUFBBack;
	TLayout *lyUFB1;
	TImage *Image1;
	TLabel *Label2;
	TMemo *Memo1;
	TLayout *lyUFB2;
	TImage *Image2;
	TLabel *Label3;
	TMemo *Memo2;
	TTabItem *tiDriverFeedBacks;
	TToolBar *tbDFB;
	TLabel *laDFB;
	TButton *buDFBBack;
	TLayout *lyDFB1;
	TImage *Image5;
	TLabel *Label8;
	TMemo *Memo5;
	TLayout *lyDFB2;
	TImage *Image6;
	TLabel *Label9;
	TMemo *Memo6;
	TTabItem *tiUserEdit;
	TToolBar *tbUE;
	TLabel *laUETitle;
	TButton *buUEBack;
	TLabel *laUERoad;
	TEdit *edUEName;
	TLabel *laUEName;
	TButton *buUERoad;
	TButton *buUESave;
	TLayout *lyUD;
	TTabItem *tiDriverEdit;
	TToolBar *tbDE;
	TLabel *laDETitle;
	TButton *buDEBack;
	TLayout *lyDE;
	TButton *buDERoad;
	TButton *buDESave;
	TEdit *edDEName;
	TLabel *laDERoad;
	TLabel *laDEName;
	TTabItem *tiUserChat;
	TToolBar *tbUC;
	TLabel *laUCTitle;
	TButton *buUCBack;
	TMemo *meUC;
	TEdit *edUC;
	TButton *buUCSend;
	TTabItem *tiDriverChat;
	TToolBar *tbDC;
	TLabel *laDCTitle;
	TButton *buDCBack;
	TMemo *meDC;
	TEdit *edDC;
	TButton *buDCSend;
	TTabItem *tiUserInfo;
	TToolBar *tbUI;
	TLabel *laUITitle;
	TButton *buUIBack;
	TLabel *laUIName;
	TButton *buUIFeedbacks;
	TButton *buUIChat;
	TImage *imUIPhoto;
	TLabel *laUIRoad;
	TLabel *laUIRoadCur;
	TButton *buUIAgree;
	TTabItem *tiUserOrders;
	TToolBar *tbUO;
	TLabel *laUOTitle;
	TButton *buUOBack;
	TLayout *lyUOOrder2;
	TLabel *Label4;
	TLabel *Label5;
	TTabItem *tiDriverOrders;
	TToolBar *ToolBar1;
	TLabel *laDOTitle;
	TButton *Button3;
	TLayout *Layout1;
	TLabel *Label7;
	TLayout *Layout2;
	TLabel *Label10;
	TLabel *Label11;
	TTabItem *tiUserPay;
	TToolBar *tbUP;
	TLabel *laUPTitle;
	TButton *buUPBack;
	TLabel *laUPCardNumber;
	TEdit *edUPCardNumber;
	TEdit *edUPFIO;
	TLabel *laUPFIO;
	TEdit *edUPDate;
	TLabel *laUPDate;
	TButton *��������;
	TLayout *lyUP;
	TStyleBook *StyleBook1;
	TPrototypeBindSource *PBS;
	TBindingsList *BindingsList1;
	TLinkListControlToField *LinkListControlToField1;
	TLinkPropertyToField *LinkPropertyToFieldBitmap;
	TLinkPropertyToField *LinkPropertyToFieldText;
	TLinkListControlToField *LinkListControlToField2;
	TLinkPropertyToField *LinkPropertyToFieldBitmap2;
	TLinkPropertyToField *LinkPropertyToFieldText3;
	TButton *Button1;
	TLayout *Layout3;
	TLabel *Label1;
	TLabel *Label6;
	TLayout *Layout4;
	TLabel *Label12;
	TLabel *Label13;
	TLayout *Layout5;
	TLabel *Label14;
	TLabel *Label15;
	void __fastcall buFRUserClick(TObject *Sender);
	void __fastcall buFRDriverClick(TObject *Sender);
	void __fastcall buFRLoginClick(TObject *Sender);
	void __fastcall buFRAboutClick(TObject *Sender);
	void __fastcall buURRegClick(TObject *Sender);
	void __fastcall buDRRegClick(TObject *Sender);
	void __fastcall buUEditClick(TObject *Sender);
	void __fastcall buULogoutClick(TObject *Sender);
	void __fastcall buUStartClick(TObject *Sender);
	void __fastcall buUOrdersClick(TObject *Sender);
	void __fastcall buDEditClick(TObject *Sender);
	void __fastcall buDLogoutClick(TObject *Sender);
	void __fastcall buDOrdersClick(TObject *Sender);
	void __fastcall buLLoginClick(TObject *Sender);
	void __fastcall swDUSwitch(TObject *Sender);
	void __fastcall laLRegClick(TObject *Sender);
	void __fastcall buLBackClick(TObject *Sender);
	void __fastcall lvUDLItemClick(TObject * const Sender, TListViewItem * const AItem);
	void __fastcall buUDLBackClick(TObject *Sender);
	void __fastcall buDIFeedbacksClick(TObject *Sender);
	void __fastcall buDIChatClick(TObject *Sender);
	void __fastcall buDIAgreeClick(TObject *Sender);
	void __fastcall buDIEditClick(TObject *Sender);
	void __fastcall buDINextClick(TObject *Sender);
	void __fastcall buUFBBackClick(TObject *Sender);
	void __fastcall buDFBBackClick(TObject *Sender);
	void __fastcall buUEBackClick(TObject *Sender);
	void __fastcall buUERoadClick(TObject *Sender);
	void __fastcall buUESaveClick(TObject *Sender);
	void __fastcall buDEBackClick(TObject *Sender);
	void __fastcall buDESaveClick(TObject *Sender);
	void __fastcall buDERoadClick(TObject *Sender);
	void __fastcall buUCBackClick(TObject *Sender);
	void __fastcall buDCBackClick(TObject *Sender);
	void __fastcall buUIBackClick(TObject *Sender);
	void __fastcall buUIFeedbacksClick(TObject *Sender);
	void __fastcall buUIChatClick(TObject *Sender);
	void __fastcall buUIAgreeClick(TObject *Sender);
	void __fastcall Button2Click(TObject *Sender);
	void __fastcall buDriverFeedBackClick(TObject *Sender);
	void __fastcall buUPBackClick(TObject *Sender);
	void __fastcall ��������Click(TObject *Sender);
	void __fastcall buUOBackClick(TObject *Sender);
	void __fastcall Button3Click(TObject *Sender);
	void __fastcall buURBackClick(TObject *Sender);
	void __fastcall buDRBackClick(TObject *Sender);
	void __fastcall buDIBackClick(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall lvDItemClick(TObject * const Sender, TListViewItem * const AItem);


private:	// User declarations
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
