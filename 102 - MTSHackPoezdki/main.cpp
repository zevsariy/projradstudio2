//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------



void __fastcall Tfm::buFRUserClick(TObject *Sender)
{
    tc->ActiveTab = tiUserReg;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buFRDriverClick(TObject *Sender)
{
    tc->ActiveTab = tiDriverReg;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buFRLoginClick(TObject *Sender)
{
    tc->ActiveTab = tiLogin;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buFRAboutClick(TObject *Sender)
{
    ShowMessage(L"������ ���������� � ������ ��������, �������� Unbreakable 16-17 ����� 2017 ����.");
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buURRegClick(TObject *Sender)
{
    tc->ActiveTab = tiUser;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buDRRegClick(TObject *Sender)
{
    tc->ActiveTab = tiDriver;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buUEditClick(TObject *Sender)
{
	tc->ActiveTab = tiUserEdit;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buULogoutClick(TObject *Sender)
{
	ShowMessage(L"�� ������� ����� �� ����� ������� ������.");
	tc->ActiveTab = tiLogin;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buUStartClick(TObject *Sender)
{
    tc->ActiveTab = tiUserDriversList;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buUOrdersClick(TObject *Sender)
{
    tc->ActiveTab = tiUserOrders;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buDEditClick(TObject *Sender)
{
	tc->ActiveTab = tiDriverEdit;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buDLogoutClick(TObject *Sender)
{
    ShowMessage(L"�� ������� ����� �� ����� ������� ������.");
	tc->ActiveTab = tiLogin;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buDOrdersClick(TObject *Sender)
{
    tc->ActiveTab = tiDriverOrders;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buLLoginClick(TObject *Sender)
{
	if(swDU->IsChecked == true)
	{
		tc->ActiveTab = tiDriver;
	}
	else
	{
		tc->ActiveTab = tiUser;
	}
}
//---------------------------------------------------------------------------

void __fastcall Tfm::swDUSwitch(TObject *Sender)
{
	if(swDU->IsChecked == true)
	{
		laDU->Text = L"����������� ��������";
	}
	else
	{
		laDU->Text = L"����������� ���������";
	}
}
//---------------------------------------------------------------------------

void __fastcall Tfm::laLRegClick(TObject *Sender)
{
    if(swDU->IsChecked == true)
	{
		tc->ActiveTab = tiDriverReg;
	}
	else
	{
		tc->ActiveTab = tiUserReg;
	}
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buLBackClick(TObject *Sender)
{
      tc->ActiveTab = tiFirstRun;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::lvUDLItemClick(TObject * const Sender, TListViewItem * const AItem)

{
    tc->ActiveTab = tiDriverInfo;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buUDLBackClick(TObject *Sender)
{
    tc->ActiveTab = tiUser;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buDIFeedbacksClick(TObject *Sender)
{
    tc->ActiveTab = tiUserFeedBacks;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buDIChatClick(TObject *Sender)
{
    tc->ActiveTab = tiUserChat;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buDIAgreeClick(TObject *Sender)
{
	ShowMessage(L"������ ������� ����������");
	PBS->Next();
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buDIEditClick(TObject *Sender)
{
	tc->ActiveTab = tiUserEdit;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buDINextClick(TObject *Sender)
{
	PBS->Next();
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buUFBBackClick(TObject *Sender)
{
    tc->ActiveTab = tiDriverInfo;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buDFBBackClick(TObject *Sender)
{
    tc->ActiveTab = tiUserInfo;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buUEBackClick(TObject *Sender)
{
    tc->ActiveTab = tiUser;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buUERoadClick(TObject *Sender)
{
	ShowMessage(L"������� ��� ������� �������!");
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buUESaveClick(TObject *Sender)
{
    tc->ActiveTab = tiUser;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buDEBackClick(TObject *Sender)
{
	tc->ActiveTab = tiDriver;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buDESaveClick(TObject *Sender)
{
	 tc->ActiveTab = tiDriver;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buDERoadClick(TObject *Sender)
{
    ShowMessage(L"������� ��� ������� �������!");
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buUCBackClick(TObject *Sender)
{
    tc->ActiveTab = tiDriverInfo;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buDCBackClick(TObject *Sender)
{
    tc->ActiveTab = tiUserInfo;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buUIBackClick(TObject *Sender)
{
    tc->ActiveTab = tiDriver;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buUIFeedbacksClick(TObject *Sender)
{
    tc->ActiveTab = tiDriverFeedBacks;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buUIChatClick(TObject *Sender)
{
    tc->ActiveTab = tiDriverChat;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buUIAgreeClick(TObject *Sender)
{
	tc->ActiveTab = tiDriver;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::Button2Click(TObject *Sender)
{
	tc->ActiveTab = tiUserPay;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buDriverFeedBackClick(TObject *Sender)
{
	ShowMessage(L"�� ������� ������� ��� �� 5 ������");
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buUPBackClick(TObject *Sender)
{
	tc->ActiveTab = tiUserOrders;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::��������Click(TObject *Sender)
{
	tc->ActiveTab = tiUserOrders;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buUOBackClick(TObject *Sender)
{
      tc->ActiveTab = tiUser;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::Button3Click(TObject *Sender)
{
	tc->ActiveTab = tiDriver;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buURBackClick(TObject *Sender)
{
	tc->ActiveTab = tiFirstRun;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buDRBackClick(TObject *Sender)
{
	tc->ActiveTab = tiFirstRun;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buDIBackClick(TObject *Sender)
{
    tc->ActiveTab = tiUserDriversList;
}
//---------------------------------------------------------------------------




void __fastcall Tfm::FormCreate(TObject *Sender)
{
    tc->ActiveTab = tiFirstRun;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::lvDItemClick(TObject * const Sender, TListViewItem * const AItem)

{
    tc->ActiveTab = tiUserInfo;
}
//---------------------------------------------------------------------------


