//---------------------------------------------------------------------------

#ifndef fmuH
#define fmuH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.Objects.hpp>
#include <FMX.Edit.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TTabControl *tc;
	TTabItem *tiStart;
	TTabItem *tiMain;
	TTabItem *tiInfo;
	TTabItem *tiSettings;
	TToolBar *tvMain;
	TLabel *laMain;
	TButton *buMainBack;
	TButton *buTestSend;
	TButton *buMainSettings;
	TImage *imStart;
	TGridPanelLayout *gplStart;
	TButton *buStart;
	TButton *buInfo;
	TButton *buExit;
	TStyleBook *sb;
	TToolBar *tbInfo;
	TLabel *laInfo;
	TButton *buInfoBack;
	TToolBar *tbSettings;
	TLabel *laSettings;
	TButton *buSettingsBack;
	TButton *buSettingsSave;
	TGridPanelLayout *gplSettings;
	TEdit *edSettingsAPIkey;
	TLabel *laSettingsAPIkey;
	TTimer *tmBalance;
	TLabel *laMainBalance;
	void __fastcall buTestSendClick(TObject *Sender);
	void __fastcall buMainSettingsClick(TObject *Sender);
	void __fastcall buInfoClick(TObject *Sender);
	void __fastcall buStartClick(TObject *Sender);
	void __fastcall buExitClick(TObject *Sender);
	void __fastcall FormSaveState(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall buMainBackClick(TObject *Sender);
	void __fastcall buSettingsSaveClick(TObject *Sender);
	void __fastcall buSettingsBackClick(TObject *Sender);
	void __fastcall buInfoBackClick(TObject *Sender);
	void __fastcall tmBalanceTimer(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
	void Tfm::RestCall(UnicodeString Method, UnicodeString Params);
	void Tfm::AddParam(UnicodeString Name, UnicodeString Value);
	UnicodeString APIkey;
	UnicodeString Tfm::GetBalance();
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
