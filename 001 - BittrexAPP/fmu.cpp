//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "fmu.h"
#include "dmu.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------

void Tfm::RestCall(UnicodeString Method, UnicodeString Params = "test")
{
	dm->rClient->BaseURL = "https://bittrex.com/api/v1.1/";
	dm->rClient->BaseURL = dm->rClient->BaseURL + Method + "/";

	if(Params != "test")
	{
		dm->rReq->AddParameter("jsonka", "fdsfsd");
	}

	dm->rReq->Execute();

	ShowMessage(dm->rResp->JSONValue->ToString());

	dm->rReq->Params->Clear();
}

void Tfm::AddParam(UnicodeString Name, UnicodeString Value)
{
	dm->rReq->AddParameter(Name, Value);
}

UnicodeString Tfm::GetBalance()
{
	dm->rClient->BaseURL = "https://bittrex.com/api/v1.1/account/getbalances";

	AddParam("apikey", APIkey);

    dm->rClient->

	dm->rReq->Execute();

	ShowMessage(dm->rResp->JSONValue->ToString());

	dm->rReq->Params->Clear();

	return "fdsfsd";
}

void __fastcall Tfm::buTestSendClick(TObject *Sender)
{
	AddParam("apikey", APIkey);
	AddParam("market", "BTC-LTC");
	//AddParam("quantity", "1.2");
	//AddParam("rate", "1.3");
	RestCall("/market/getopenorders");
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buMainSettingsClick(TObject *Sender)
{
	tc->ActiveTab = tiSettings;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buInfoClick(TObject *Sender)
{
     tc->ActiveTab = tiInfo;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buStartClick(TObject *Sender)
{
	tc->ActiveTab = tiMain;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buExitClick(TObject *Sender)
{
	Close();
}
//---------------------------------------------------------------------------

void __fastcall Tfm::FormSaveState(TObject *Sender)
{
    TBinaryWriter* x;
	SaveState->Stream->Clear();
	x = new TBinaryWriter(SaveState->Stream);
	try {
		x->Write(APIkey);
	}
	__finally {
		x->DisposeOf();
	}
}
//---------------------------------------------------------------------------

void __fastcall Tfm::FormCreate(TObject *Sender)
{
    TBinaryReader* x;
	if (SaveState->Stream->Size > 0)
	{
		x = new TBinaryReader(SaveState->Stream, TEncoding::UTF8, false);
		try
		{
			APIkey = x->ReadString();
			edSettingsAPIkey->Text = APIkey;
			if(APIkey != "");
			{
                tmBalance->Enabled = true;
            }
		}
		__finally {
            x->DisposeOf();
        }
	}
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buMainBackClick(TObject *Sender)
{
    tc->ActiveTab = tiStart;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buSettingsSaveClick(TObject *Sender)
{
    APIkey = edSettingsAPIkey->Text;
	tc->ActiveTab = tiMain;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buSettingsBackClick(TObject *Sender)
{
	tc->ActiveTab = tiMain;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buInfoBackClick(TObject *Sender)
{
    tc->ActiveTab = tiStart;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::tmBalanceTimer(TObject *Sender)
{
   laMainBalance->Text = GetBalance();
   tmBalance->Enabled = false;
}
//---------------------------------------------------------------------------

