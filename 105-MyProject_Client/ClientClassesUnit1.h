#ifndef ClientClassesUnit1H
#define ClientClassesUnit1H

#include "Data.DBXCommon.hpp"
#include "System.Classes.hpp"
#include "System.SysUtils.hpp"
#include "Data.DB.hpp"
#include "Data.SqlExpr.hpp"
#include "Data.DBXDBReaders.hpp"
#include "Data.DBXCDSReaders.hpp"
#include "DataSnap.DSProxyRest.hpp"

  class TServerMethods1Client : public TDSAdminRestClient
  {
  private:
    TDSRestCommand *FEchoStringCommand;
    TDSRestCommand *FReverseStringCommand;
    TDSRestCommand *FEcho2Command;
  public:
    __fastcall TServerMethods1Client(TDSRestConnection *ARestConnection);
    __fastcall TServerMethods1Client(TDSRestConnection *ADBXConnection, bool AInstanceOwner);
    __fastcall ~TServerMethods1Client();
    System::UnicodeString __fastcall EchoString(System::UnicodeString value, const String& ARequestFilter = String());
    System::UnicodeString __fastcall ReverseString(System::UnicodeString value, const String& ARequestFilter = String());
    System::UnicodeString __fastcall Echo2(System::UnicodeString value, const String& ARequestFilter = String());
  };

#endif
