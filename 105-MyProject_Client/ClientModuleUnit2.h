//----------------------------------------------------------------------------

#ifndef ClientModuleUnit2H
#define ClientModuleUnit2H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include "ClientClassesUnit1.h"
#include <Datasnap.DSClientRest.hpp>
//----------------------------------------------------------------------------
class TClientModule2 : public TDataModule
{
__published:	// IDE-managed Components
	TDSRestConnection *DSRestConnection1;
private:	// User declarations
	bool FInstanceOwner;
	TServerMethods1Client* FServerMethods1Client;
	TServerMethods1Client* GetServerMethods1Client(void);
public:		// User declarations
	__fastcall TClientModule2(TComponent* Owner);
	__fastcall ~TClientModule2();
	__property bool InstanceOwner = {read=FInstanceOwner, write=FInstanceOwner};
	__property TServerMethods1Client* ServerMethods1Client = {read=GetServerMethods1Client, write=FServerMethods1Client};
};
//---------------------------------------------------------------------------
extern PACKAGE TClientModule2 *ClientModule2;
//---------------------------------------------------------------------------
#endif
