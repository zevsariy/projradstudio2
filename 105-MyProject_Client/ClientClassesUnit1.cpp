// 
// Created by the DataSnap proxy generator.
// 22.05.2017 15:38:35
// 

#include "ClientClassesUnit1.h"

  struct TDSRestParameterMetaData TServerMethods1_EchoString[] =
  {
    {"value", 1, 26, "String"},
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TServerMethods1_ReverseString[] =
  {
    {"value", 1, 26, "String"},
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TServerMethods1_Echo2[] =
  {
    {"value", 1, 26, "String"},
    {"", 4, 26, "String"}
  };

System::UnicodeString __fastcall TServerMethods1Client::EchoString(System::UnicodeString value, const String& ARequestFilter)
{
  if (FEchoStringCommand == NULL)
  {
    FEchoStringCommand = FConnection->CreateCommand();
    FEchoStringCommand->RequestType = "GET";
    FEchoStringCommand->Text = "TServerMethods1.EchoString";
    FEchoStringCommand->Prepare(TServerMethods1_EchoString, 1);
  }
  FEchoStringCommand->Parameters->Parameter[0]->Value->SetWideString(value);
  FEchoStringCommand->Execute(ARequestFilter);
  System::UnicodeString result = FEchoStringCommand->Parameters->Parameter[1]->Value->GetWideString();
  return result;
}

System::UnicodeString __fastcall TServerMethods1Client::ReverseString(System::UnicodeString value, const String& ARequestFilter)
{
  if (FReverseStringCommand == NULL)
  {
    FReverseStringCommand = FConnection->CreateCommand();
    FReverseStringCommand->RequestType = "GET";
    FReverseStringCommand->Text = "TServerMethods1.ReverseString";
    FReverseStringCommand->Prepare(TServerMethods1_ReverseString, 1);
  }
  FReverseStringCommand->Parameters->Parameter[0]->Value->SetWideString(value);
  FReverseStringCommand->Execute(ARequestFilter);
  System::UnicodeString result = FReverseStringCommand->Parameters->Parameter[1]->Value->GetWideString();
  return result;
}

System::UnicodeString __fastcall TServerMethods1Client::Echo2(System::UnicodeString value, const String& ARequestFilter)
{
  if (FEcho2Command == NULL)
  {
    FEcho2Command = FConnection->CreateCommand();
    FEcho2Command->RequestType = "GET";
    FEcho2Command->Text = "TServerMethods1.Echo2";
    FEcho2Command->Prepare(TServerMethods1_Echo2, 1);
  }
  FEcho2Command->Parameters->Parameter[0]->Value->SetWideString(value);
  FEcho2Command->Execute(ARequestFilter);
  System::UnicodeString result = FEcho2Command->Parameters->Parameter[1]->Value->GetWideString();
  return result;
}


__fastcall  TServerMethods1Client::TServerMethods1Client(TDSRestConnection *ARestConnection): TDSAdminRestClient(ARestConnection)
{
}

__fastcall  TServerMethods1Client::TServerMethods1Client(TDSRestConnection *ARestConnection, bool AInstanceOwner): TDSAdminRestClient(ARestConnection, AInstanceOwner)
{
}

__fastcall  TServerMethods1Client::~TServerMethods1Client()
{
  delete FEchoStringCommand;
  delete FReverseStringCommand;
  delete FEcho2Command;
}

