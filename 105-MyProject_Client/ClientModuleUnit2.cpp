//----------------------------------------------------------------------------

#pragma hdrstop
#include <stdio.h>
#include <memory>

#include "ClientModuleUnit2.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma classgroup "FMX.Controls.TControl"
#pragma resource "*.dfm"
TClientModule2 *ClientModule2;
//---------------------------------------------------------------------------
__fastcall TClientModule2::TClientModule2(TComponent* Owner)
	: TDataModule(Owner)
{
	FInstanceOwner = true;
}

__fastcall TClientModule2::~TClientModule2()
{
	delete FServerMethods1Client;
}

TServerMethods1Client* TClientModule2::GetServerMethods1Client(void)
{
	if (FServerMethods1Client == NULL)
		FServerMethods1Client= new TServerMethods1Client(DSRestConnection1, FInstanceOwner);
	return FServerMethods1Client;
};

