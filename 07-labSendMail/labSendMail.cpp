//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "labSendMail.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buSendClick(TObject *Sender)
{
	IdSMTP->Host = "smtp.mail.ru";
	IdSMTP->Port = 587;
	IdSMTP->Username = "test12321@mail.ru";
	IdSMTP->Password = "Pa$$w0rd";
	IdSMTP->UseTLS = utUseExplicitTLS;
	IdSMTP->ReadTimeout = 15000;
	IdSMTP->Connect();
	if (IdSMTP->Connected() == true){
		TIdMessage *x = new TIdMessage();
		try {
			//From->Name = Trin (__);
			x->From->Address = IdSMTP->Username;
			x->Recipients->EMailAddresses = Trim(edTo->Text);
			x->Subject = Trim(edSubject->Text);
			x->Body->Assign(meBody->Lines);
			x->CharSet = "Windows-1251";
			if (ckIncludeAtt->IsChecked) {
				UnicodeString xFileName = Ioutils::TPath::ChangeExtension(
					Ioutils::TPath::GetTempFileName(), ".txt");
				meBody->Lines->SaveToFile(xFileName, TEncoding::UTF8);
				new TIdAttachmentFile(x->MessageParts, xFileName);
			}
			IdSMTP->Send(x);
			ShowMessage(L"������ ����������");
		}
		__finally{
			delete x;
            IdSMTP->Disconnect();
        }
	}
}
//---------------------------------------------------------------------------
