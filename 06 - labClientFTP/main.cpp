//---------------------------------------------------------------------------

#include <fmx.h>
#include <System.IOUtils.hpp>
#pragma hdrstop

#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buConnectClick(TObject *Sender)
{
	IdFTP->Host = "46.188.34.242";
	IdFTP->Username = "test";
	IdFTP->Password = "test";
	IdFTP->Passive = true;
	IdFTP->Connect();
	IdFTP->ChangeDir("/ftp/");
	IdFTP->List(lb->Items, "", false);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::lbItemClick(TCustomListBox * const Sender, TListBoxItem * const Item)

{
	UnicodeString xExt = Ioutils::TPath::GetExtension(Item->Text);
	TMemoryStream *x = new TMemoryStream();
	if(IdFTP->Connected() == false)
	{
        IdFTP->Connect();
    }
	try
	{
		mp->Stop();
        mp->Clear();
		if( xExt == ".txt")
		{
			tc->ActiveTab = tiText;
			IdFTP->Get(Item->Text, x, true);
			x->Seek(0,0);
			me->Lines->LoadFromStream(x);
		}
		else if(xExt == ".png" || xExt == ".jpg")
		{
			tc->ActiveTab = tiImage;
			IdFTP->Get(Item->Text, x, true);
			im->Bitmap->LoadFromStream(x);
		}
		else if(xExt == ".mp4" || xExt == ".mp4")
		{
			tc->ActiveTab = tiPlayer;
			IdFTP->Get(Item->Text, x, true);
			x->SaveToFile("temp.mp4");
			mp->FileName = "temp.mp4";
            mp->Play();
		}
		else
		{
			tc->ActiveTab = tiOther;
        }
	}
	__finally
	{
        delete x;
    }
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buUploadClick(TObject *Sender)
{
	TMemoryStream *x = new TMemoryStream();
	 try
	 {
		Image->Bitmap->SaveToStream(x);
		IdFTP->Put(x,"001-pashalka" + IntToStr(random(1000)) + ".jpg");
		ShowMessage(L"���� ������� ��������");
		IdFTP->Disconnect();
		IdFTP->Connect();
	 }
	 __finally
	 {
		 delete x;
	 }

}
//---------------------------------------------------------------------------
void __fastcall Tfm::buRefreshClick(TObject *Sender)
{
	 IdFTP->Disconnect();
	 buConnectClick(Sender);
}
//---------------------------------------------------------------------------
