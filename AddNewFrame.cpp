//-------------------------------------------------------------------------— 
void __fastcall Tfm::ProductCellOnClick(TObject * Sender) {
    ShowProduct(((TControl * ) Sender) - > Tag);
  }
  //-------------------------------------------------------------------------— 
void Tfm::ReloadList() {
    dm - > cdsProduct - > First();
    while (!dm - > cdsProduct - > Eof) {
      TfrProductCell * x = new TfrProductCell(glList);
      x - > Parent = glList;
      x - > Align = TAlignLayout::Client;
      x - > Name = "frProductCell" + IntToStr(dm - > cdsProductID - > Value);
      x - > laCaption - > Text = dm - > cdsProductNAME - > Value;
      x - > laPrice - > Text = FloatToStr(dm - > cdsProductPRICE - > Value) + L " руб";
      x - > im - > Bitmap - > Assign(dm - > cdsProductIMAGE);
      x - > Tag = dm - > cdsProductID - > Value;
      x - > OnClick = ProductCellOnClick;
      dm - > cdsProduct - > Next();
    }
    glList - > RecalcSize();
  }
  //-------------------------------------------------------------------------— 
void __fastcall Tfm::buProductListClick(TObject * Sender) {
    ShowProductList();
  }
  //-------------------------------------------------------------------------— 
void __fastcall Tfm::FormShow(TObject * Sender) {
    ReloadList();
  }
  //-------------------------------------------------------------------------— 
void __fastcall Tfm::glListResize(TObject * Sender) {
    // увеличиваем размеры плитки 
    float xNewWidth = (glList - > Width < 400) ? glList - > Width : glList - > Width / 2;
    glList - > ItemHeight = (glList - > ItemHeight * xNewWidth) / glList - > ItemWidth;
    glList - > ItemWidth = xNewWidth;
    // Корректируем высоту компонента, что бы нужной длины был скрол 
    int x = Ceil((glList - > ComponentCount - 1) / (glList - > Width / glList - > ItemWidth));
    glList - > Height = x * glList - > ItemHeight;
  }
  //-------------------------------------------------------------------------— 
void Tfm::ShowProductList() {
    tc - > GotoVisibleTab(tiList - > Index);
  }
  //-------------------------------------------------------------------------— 
void Tfm::ShowProduct(int aID) {
  TLocateOptions xLO;
  dm - > cdsProduct - > Locate(dm - > cdsProductID - > FieldName, aID, xLO);
  // 
  laProductName - > Text = dm - > cdsProductNAME - > Value;
  imProductImage - > Bitmap - > Assign(dm - > cdsProductIMAGE);
  laProductWeight - > Text = FormatFloat("0.000", dm - > cdsProductWEIGHT - > Value);
  laProductPrice - > Text = FloatToStr(dm - > cdsProductPRICE - > Value) + L " руб";
  txProductNotes - > Text = dm - > cdsProductNOTES - > Value;
  // 
  tc - > GotoVisibleTab(tiProduct - > Index);
}