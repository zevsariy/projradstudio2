//---------------------------------------------------------------------------

#ifndef mediapleerH
#define mediapleerH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <FMX.ActnList.hpp>
#include <FMX.Dialogs.hpp>
#include <FMX.Media.hpp>
#include <FMX.Memo.hpp>
#include <FMX.ScrollBox.hpp>
#include <IPPeerClient.hpp>
#include <IPPeerServer.hpp>
#include <System.Actions.hpp>
#include <System.Tether.AppProfile.hpp>
#include <System.Tether.Manager.hpp>
#include <FMX.StdActns.hpp>
#include <FMX.Media.hpp>
//---------------------------------------------------------------------------
class Tfm1 : public TForm
{
__published:	// IDE-managed Components
	TToolBar *ToolBar1;
	TButton *buOpen;
	TButton *buPlay;
	TButton *buStop;
	TButton *buInfo;
	TButton *buClear;
	TLayout *lyVolume;
	TLabel *laVolume;
	TLabel *laVolumeCaption;
	TTrackBar *tbVolume;
	TMemo *meLog;
	TLayout *lyTime;
	TTrackBar *tbTime;
	TLabel *laTime;
	TLabel *laDuration;
	TMediaPlayer *mp;
	TMediaPlayerControl *mpc;
	TActionList *al;
	TOpenDialog *od;
	TTetheringManager *ttm;
	TTetheringAppProfile *ttp;
	TMediaPlayerStop *acMediaPlayerStop;
	TMediaPlayerPlayPause *acMediaPlayerPlayPause;
	TMediaPlayerCurrentTime *acMediaPlayerCurrentTime;
	TMediaPlayerVolume *acMediaPlayerVolume;
	TAction *acClear;
	TAction *acPlayPause;
	TAction *acStop;
	void __fastcall buOpenClick(TObject *Sender);
	void __fastcall acPlayPauseExecute(TObject *Sender);
	void __fastcall acStopExecute(TObject *Sender);
	void __fastcall tbVolumeChange(TObject *Sender);
	void __fastcall tbTimeChange(TObject *Sender);
	void __fastcall ttpResourceReceived(TObject * const Sender, TRemoteResource * const AResource);

private:	// User declarations
public:		// User declarations
	__fastcall Tfm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm1 *fm1;
//---------------------------------------------------------------------------
#endif
