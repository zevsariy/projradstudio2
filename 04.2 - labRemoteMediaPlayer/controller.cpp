//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "controller.h"
#include "mediapleer.h"
#include "control.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm2 *fm2;
//---------------------------------------------------------------------------
__fastcall Tfm2::Tfm2(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tfm2::FormShow(TObject *Sender)
{
	ttm->DiscoverManagers();
}
//---------------------------------------------------------------------------

void __fastcall Tfm2::buFindClick(TObject *Sender)
{
    ttm->DiscoverManagers();
}
//---------------------------------------------------------------------------

void __fastcall Tfm2::ttmEndProfilesDiscovery(TObject * const Sender, TTetheringProfileInfoList * const ARemoteProfiles)

{
	lb->Clear();
	for (int i = 0; i < ttm->RemoteProfiles->Count; i++) {
		lb->Items->Add(ttm->RemoteProfiles->Items[i].ProfileText);
	}
	if (lb->Count > 0) {
		lb->ItemIndex = 0;
        ttp->Connect(ttm->RemoteProfiles->Items[lb->ItemIndex]);

	}
}
//---------------------------------------------------------------------------

void __fastcall Tfm2::ttmEndManagersDiscovery(TObject * const Sender, TTetheringManagerInfoList * const ARemoteManagers)

{
	for (int i = 0; i <ARemoteManagers->Count; i++) {
		ttm->PairManager(
			const_cast<TTetheringManagerInfoList*>(ARemoteManagers)->Items[i]);
	}
}
//---------------------------------------------------------------------------


void __fastcall Tfm2::ttpResources0ResourceReceived(TObject * const Sender, TRemoteResource * const AResource)

{
    tbVolume->Value = AResource->Value.AsSingle;
}
//---------------------------------------------------------------------------

void __fastcall Tfm2::ttpResources1ResourceReceived(TObject * const Sender, TRemoteResource * const AResource)

{
    tbVolume->Max = AResource->Value.AsSingle;
}
//---------------------------------------------------------------------------

void __fastcall Tfm2::ttpResources2ResourceReceived(TObject * const Sender, TRemoteResource * const AResource)

{
    tbTime->Value = AResource->Value.AsSingle;
}
//---------------------------------------------------------------------------

void __fastcall Tfm2::ttpResources3ResourceReceived(TObject * const Sender, TRemoteResource * const AResource)

{
    tbTime->Max = AResource->Value.AsSingle;
}
//---------------------------------------------------------------------------

void __fastcall Tfm2::buSendStringClick(TObject *Sender)
{
    ttp->SendString(ttm->RemoteProfiles->Items[lb->ItemIndex], "ABCD", edString->Text);
}
//---------------------------------------------------------------------------

