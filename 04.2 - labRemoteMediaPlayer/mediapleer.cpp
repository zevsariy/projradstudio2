//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "mediapleer.h"
#include "media.h"
#include "controller.h"
#include "control.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm1 *fm1;
//---------------------------------------------------------------------------
__fastcall Tfm1::Tfm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------

void __fastcall Tfm1::buOpenClick(TObject *Sender)
{
	od->Filter = TMediaCodecManager::GetFilterString();
	if (od->Execute()) {
		mp->FileName = od->FileName;
//      laDuration->Text = " ____ //TODO
	}
}
//---------------------------------------------------------------------------


void __fastcall Tfm1::acPlayPauseExecute(TObject *Sender)
{
    acMediaPlayerPlayPause->Execute();
}
//---------------------------------------------------------------------------

void __fastcall Tfm1::acStopExecute(TObject *Sender)
{
    acMediaPlayerStop->Execute();
}
//---------------------------------------------------------------------------

void __fastcall Tfm1::tbVolumeChange(TObject *Sender)
{
	ttp->Resources->FindByName("Volume")->Value = tbVolume->Value;
    ttp->Resources->FindByName("VolumeMax")->Value = tbVolume->Max;
}
//---------------------------------------------------------------------------

void __fastcall Tfm1::tbTimeChange(TObject *Sender)
{
	ttp->Resources->FindByName("Time")->Value = tbTime->Value;
	ttp->Resources->FindByName("TimeMax")->Value = tbTime->Max;
}
//---------------------------------------------------------------------------

void __fastcall Tfm1::ttpResourceReceived(TObject * const Sender, TRemoteResource * const AResource)

{
    meLog->Lines->Add(AResource->Hint + " = " + AResource->Value.AsString);
}
//---------------------------------------------------------------------------

