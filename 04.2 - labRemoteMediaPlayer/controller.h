//---------------------------------------------------------------------------

#ifndef controllerH
#define controllerH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.ActnList.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Edit.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.ListBox.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <IPPeerClient.hpp>
#include <IPPeerServer.hpp>
#include <System.Actions.hpp>
#include <System.Tether.AppProfile.hpp>
#include <System.Tether.Manager.hpp>
#include <FMX.Media.hpp>
//---------------------------------------------------------------------------
class Tfm2 : public TForm
{
__published:	// IDE-managed Components
	TToolBar *ToolBar1;
	TButton *buFind;
	TLayout *Layout1;
	TLabel *Label1;
	TLabel *Label2;
	TLayout *Layout2;
	TButton *buPlay;
	TButton *buStop;
	TButton *buClear;
	TLabel *Label3;
	TTrackBar *tbVolume;
	TLabel *Label4;
	TTrackBar *tbTime;
	TButton *buSendString;
	TListBox *lb;
	TEdit *edString;
	TTetheringManager *ttm;
	TTetheringAppProfile *ttp;
	TActionList *al;
	TAction *acPlayPause;
	TAction *acStop;
	TAction *acClear;
	void __fastcall FormShow(TObject *Sender);
	void __fastcall buFindClick(TObject *Sender);
	void __fastcall ttmEndProfilesDiscovery(TObject * const Sender, TTetheringProfileInfoList * const ARemoteProfiles);
	void __fastcall ttmEndManagersDiscovery(TObject * const Sender, TTetheringManagerInfoList * const ARemoteManagers);
	void __fastcall ttpResources0ResourceReceived(TObject * const Sender, TRemoteResource * const AResource);
	void __fastcall ttpResources1ResourceReceived(TObject * const Sender, TRemoteResource * const AResource);
	void __fastcall ttpResources2ResourceReceived(TObject * const Sender, TRemoteResource * const AResource);
	void __fastcall ttpResources3ResourceReceived(TObject * const Sender, TRemoteResource * const AResource);
	void __fastcall buSendStringClick(TObject *Sender);




private:	// User declarations
public:		// User declarations
	__fastcall Tfm2(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm2 *fm2;
//---------------------------------------------------------------------------
#endif
